package com.kalemao.library.base;

import android.content.Context;
import android.graphics.Typeface;
import com.kalemao.library.custom.photopicker.model.ImageItem;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 基础的运行时数据管理类
 *
 * @author dim.
 * @time 2017/11/15 11:13.
 */
public class RunTimeData {
    protected static RunTimeData             runTimeData;
    /**
     * 网络请求的httphead
     */
    private          HashMap<String, String> httpHead;
    /**
     * 全局的context
     */
    private          Context                 mContext;
    /**
     * 通用的报错提示
     */
    private String  baseErrorMessage = "Network connection is abnormal. Please check the network";
    /**
     * 是否需要去聊天
     */
    private boolean doesNeedGotoTalk = false;
    /**
     * 返回的选择的图片
     */
    private ArrayList<ImageItem> choseImage;
    /**
     * 基础的字体
     */
    private Typeface             textTypeNormal;
    /**
     * 加粗一点的那个字体
     */
    private Typeface             textTypeBlood;

    /**
     * 政策数量
     */
    private int intpolicy_size = 0;

    /**
     * 喵秘跳转卡乐猫商品详情专用
     */
    private String miaomi_spu_sn;

    /**
     * 是否需要刷新视屏页面的数据
     */
    private boolean doesNeedRefreshVideo = false;

    /**
     * 是否需要提示视频网络状态变更
     */
    private boolean doesNeedShowWifiChangedForVideo = true;

    /**
     * 返回按钮的icon
     */
    private String iconBack;

    /**
     * 是否可以添加音乐
     */
    private Boolean doesCanAddMusic = true;

    public String getIconBack() {
        return iconBack;
    }

    public void setIconBack(String iconBack) {
        this.iconBack = iconBack;
    }

    public boolean isDoesNeedShowWifiChangedForVideo() {
        return doesNeedShowWifiChangedForVideo;
    }

    public void setDoesNeedShowWifiChangedForVideo(boolean doesNeedShowWifiChangedForVideo) {
        this.doesNeedShowWifiChangedForVideo = doesNeedShowWifiChangedForVideo;
    }

    public boolean isDoesNeedRefreshVideo() {
        return doesNeedRefreshVideo;
    }

    public void setDoesNeedRefreshVideo(boolean doesNeedRefreshVideo) {
        this.doesNeedRefreshVideo = doesNeedRefreshVideo;
    }

    public static RunTimeData getInstance() {
        if (runTimeData == null) {
            runTimeData = new RunTimeData();
        }
        return runTimeData;
    }

    /**
     * 买家首页轮播图。因为第三方控件，滚动和recyclerview冲突了。时间问题快速解决下，这样恶心的用个变量先控制了，等有空的时候在修改
     */
    private boolean doesCanScroll = true;

    public boolean isDoesCanScroll() {
        return doesCanScroll;
    }

    public void setDoesCanScroll(boolean doesCanScroll) {
        this.doesCanScroll = doesCanScroll;
    }

    public int getIntpolicy_size() {
        return intpolicy_size;
    }

    public void setIntpolicy_size(int intpolicy_size) {
        this.intpolicy_size = intpolicy_size;
    }

    public String getMiaomi_spu_sn() {
        return miaomi_spu_sn;
    }

    public void setMiaomi_spu_sn(String miaomi_spu_sn) {
        this.miaomi_spu_sn = miaomi_spu_sn;
    }

    public Typeface getTextTypeNormal() {
        return textTypeNormal;
    }

    public void setTextTypeNormal(Typeface textTypeNormal) {
        this.textTypeNormal = textTypeNormal;
    }

    public Typeface getTextTypeBlood() {
        return textTypeBlood;
    }

    public void setTextTypeBlood(Typeface textTypeBlood) {
        this.textTypeBlood = textTypeBlood;
    }

    public boolean isDoesNeedGotoTalk() {
        return doesNeedGotoTalk;
    }

    public void setDoesNeedGotoTalk(boolean doesNeedGotoTalk) {
        this.doesNeedGotoTalk = doesNeedGotoTalk;
    }

    public HashMap<String, String> getHttpHead() {
        return httpHead == null ? new HashMap<>() : httpHead;
    }

    public void setHttpHead(HashMap<String, String> httpHead) {
        this.httpHead = httpHead;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public String getBaseErrorMessage() {
        return baseErrorMessage;
    }

    public void setBaseErrorMessage(String baseErrorMessage) {
        this.baseErrorMessage = baseErrorMessage;
    }

    public ArrayList<ImageItem> getChoseImage() {
        return choseImage;
    }

    public void setChoseImage(ArrayList<ImageItem> choseImage) {
        this.choseImage = choseImage;
    }

    public Boolean getDoesCanAddMusic() {
        return doesCanAddMusic;
    }

    public void setDoesCanAddMusic(Boolean doesCanAddMusic) {
        this.doesCanAddMusic = doesCanAddMusic;
    }
}
