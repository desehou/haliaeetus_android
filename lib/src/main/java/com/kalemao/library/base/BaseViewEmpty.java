package com.kalemao.library.base;

import com.kalemao.library.R;
import com.kalemao.library.imageview.KLMImageView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 空页面 Created by dim on 2017/6/22 19:04 邮箱：271756926@qq.com
 */

public class BaseViewEmpty extends LinearLayout {
    private Context      mContext;
    private KLMImageView icon;
    private TextView     desTop, desBottom;

    public BaseViewEmpty(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public void initView() {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.view_empty, this, true);
        layout.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        icon = (KLMImageView) layout.findViewById(R.id.view_empty_img);
        desTop = (TextView) layout.findViewById(R.id.view_empty_des_top);
        desBottom = (TextView) layout.findViewById(R.id.view_empty_des_bottom);
    }

    public void setShowDes(String topDes, String bottomDes) {
        desTop.setText(topDes);
        desBottom.setText(bottomDes);
    }

    public void setIcon(int iconRes) {
        icon.setImageUrl(mContext, iconRes);
    }
}
