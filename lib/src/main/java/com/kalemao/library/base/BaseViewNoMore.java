package com.kalemao.library.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.kalemao.library.R;

/**
 * Created by dim on 2017/6/22 19:04 邮箱：271756926@qq.com
 */

public class BaseViewNoMore extends LinearLayout {
    private Context      mContext;
    private LinearLayout mRootView;
    private boolean      mDoesNeedWhiteBg;

    public BaseViewNoMore(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public BaseViewNoMore(Context context, boolean needWhiteBg) {
        super(context);
        this.mContext = context;
        mDoesNeedWhiteBg = needWhiteBg;
        initView();
    }

    public void initView() {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.view_nomore, this, true);
        layout.setLayoutParams(
                new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mRootView = (LinearLayout) layout.findViewById(R.id.view_nomore_root);
        if (mDoesNeedWhiteBg) {
            mRootView.setBackgroundResource(R.color.white);
        } else {
            mRootView.setBackgroundResource(R.color.klm_F5);
        }
    }
}
