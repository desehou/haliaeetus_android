package com.kalemao.library.http;

import android.content.Intent;
import com.kalemao.library.R;
import com.kalemao.library.base.RunTimeData;
import com.kalemao.library.logutils.LogUtil;
import com.kalemao.library.utils.BaseToast;
import com.kalemao.library.utils.PackageUtil;
import retrofit2.HttpException;
import rx.Observer;

/**
 * 网络请求返回数据状态通用解析
 *
 * Created by dim on 2017/7/2 10:52 邮箱：271756926@qq.com
 */

public abstract class BaseObserver<T> implements Observer<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        LogUtil.e("BaseObserver", e.getMessage());
        // todo error somthing

        handleException(e);
    }

    @Override
    public void onNext(T t) {
        LogUtil.e("BaseObserver", "");
    }

    private static final int UNAUTHORIZED          = 401;
    private static final int FORBIDDEN             = 403;
    private static final int NOT_FOUND             = 404;
    private static final int REQUEST_TIMEOUT       = 408;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int BAD_GATEWAY           = 502;
    private static final int SERVICE_UNAVAILABLE   = 503;
    private static final int GATEWAY_TIMEOUT       = 504;

    public Exception handleException(Throwable e) {
        Exception ex;
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            ex = new Exception();
            LogUtil.d("App", "httpException.code() == " + httpException.code());
            switch (httpException.code()) {
                case UNAUTHORIZED:
                    ex = new Exception(
                            RunTimeData.getInstance().getmContext().getResources().getString(R.string.login_again));
                    // 401错误，需要重新登陆
                    if (PackageUtil.doesMMForMYApp(RunTimeData.getInstance().getmContext())) {
                        Intent intent = new Intent();
                        intent.setClassName(RunTimeData.getInstance().getmContext(),
                                            "com.ewanse.malaysia_miaomi.ui.login.LoginActivity");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("from_code_401", true);
                        RunTimeData.getInstance().getmContext().startActivity(intent);
                    } else if (PackageUtil.doesKLMForMYApp(RunTimeData.getInstance().getmContext())) {
                        Intent intent = new Intent();
                        intent.setClassName(RunTimeData.getInstance().getmContext(),
                                            "com.kalemao.aphrodite.ui.login.LoginActivity");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("from_code_401", true);
                        RunTimeData.getInstance().getmContext().startActivity(intent);
                    } else if (PackageUtil.doesAppFoyZDYP(RunTimeData.getInstance().getmContext())) {
                        Intent intent = new Intent();
                        intent.setClassName(RunTimeData.getInstance().getmContext(), "com.ewanse.zdyp.ui.login.Login");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("from_code_401", true);
                        RunTimeData.getInstance().getmContext().startActivity(intent);
                    }
                    break;
                case INTERNAL_SERVER_ERROR:
                    BaseToast.showShort(RunTimeData.getInstance().getmContext(),"网络不给力，请稍后再试");
                    break;
                case NOT_FOUND:
                case REQUEST_TIMEOUT:
                case GATEWAY_TIMEOUT:
                case BAD_GATEWAY:
                case SERVICE_UNAVAILABLE:
                default:
                    // ex.message = "网络错误";
                    break;
            }
            return ex;
        }
        return new Exception();
    }
}
