package com.kalemao.library.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kalemao.library.base.MResponse;
import java.util.Date;

/**
 * @author by dim
 * @data 2017/12/6 17:15
 * 邮箱：271756926@qq.com
 */

public class JsonUtils {
    public static <T> T fromJsonDate(String jsonString, Class<T> type) throws Exception {
        GsonBuilder gsonb = new GsonBuilder();
        DateDeserializer dds = new DateDeserializer();
        gsonb.registerTypeAdapter(Date.class, dds);
        Gson gson = gsonb.create();
        return gson.fromJson(jsonString, type);
    }

    /**
     * 对象转换成json字符串
     *
     * @param obj
     * @return
     */
    public static String toJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    /**
     * 验证是否成功 response.biz_action=0
     *
     * @param response
     * @return
     */
    public static Boolean ValidateResult(MResponse response) {
        Boolean bReturn = true;
        if (response.getBiz_action() == 1) {
            bReturn = false;
        }
        return bReturn;
    }
}
