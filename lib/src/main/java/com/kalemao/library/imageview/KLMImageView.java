package com.kalemao.library.imageview;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kalemao.library.R;
import com.kalemao.library.base.RunTimeData;
import com.kalemao.library.imageview.glide.GlideRoundTransform;
import java.io.File;

/**
 * 通用的图片加载ImageView
 *
 * Created by dim on 2017/5/16 16:40 邮箱：271756926@qq.com
 */

public class KLMImageView extends AppCompatImageView {

    protected int mPlaceholderImageResouce; // 占位图,不设置占位图，默认显示菊花
    protected int mFailureImageResouce;     // 失败的图片

    protected void initPlaceholderImage() {
        if (mPlaceholderImageResouce == 0) {
            mPlaceholderImageResouce = R.drawable.klm_default_iamge;
        }
    }

    protected void initFailureImage() {
        if (mFailureImageResouce == 0) {
            mFailureImageResouce = R.drawable.img_loading_failed;
        }
    }

    protected void initKLMImageViewDefault() {
        initPlaceholderImage();
        initFailureImage();
    }

    public KLMImageView(Context context) {
        super(context);
        initKLMImageViewDefault();
    }

    public KLMImageView(Context context, int placeholderImageResouce, int failureImageResouce) {
        super(context);
        this.mFailureImageResouce = failureImageResouce;
        this.mPlaceholderImageResouce = placeholderImageResouce;
    }

    public KLMImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initKLMImageViewDefault();
    }

    public KLMImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initKLMImageViewDefault();
    }

    public void setImageUrl(Context context, String url) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .apply(new RequestOptions().centerCrop()
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .skipMemoryCache(false)
                               .dontAnimate()
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    public void setImageUrl(String url) {
        setImageUrl(RunTimeData.getInstance().getmContext(), url);
    }

    public void setImageUrlCorner(String url) {
        Glide.with(RunTimeData.getInstance().getmContext().getApplicationContext()).load(url).apply(
                new RequestOptions().error(mFailureImageResouce)
                        .placeholder(mPlaceholderImageResouce)
                        .skipMemoryCache(false)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new GlideRoundTransform(RunTimeData.getInstance().getmContext(), 8))).into(this);
    }

    public void setImageUrl(Context context, String url, int imageWid, int imageHei) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .apply(new RequestOptions().centerCrop()
                               .override(imageWid, imageHei)
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .skipMemoryCache(false)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    public void setImageUrlCenterInside(Context context, String url) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .apply(new RequestOptions().centerInside()
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .skipMemoryCache(false)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    public void setImageUrl(int res) {
        setImageUrl(RunTimeData.getInstance().getmContext(), res);
    }

    public void setImageUrl(Context context, int res) {
        Glide.with(context.getApplicationContext())
                .load(res)
                .apply(new RequestOptions().centerCrop()
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .skipMemoryCache(false)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    public void setImageUrl(Context context, int res, int imageWid, int imageHei) {
        Glide.with(context.getApplicationContext())
                .load(res)
                .apply(new RequestOptions().centerCrop()
                               .override(imageWid, imageHei)
                               .error(mFailureImageResouce)
                               .skipMemoryCache(false)
                               .placeholder(mPlaceholderImageResouce)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    public void setImageFilePath(String path) {
        setImageFilePath(RunTimeData.getInstance().getmContext(), path);
    }

    public void setImageFilePath(Context context, String path) {
        Glide.with(context.getApplicationContext()).load(new File("file://" + path)).apply(
                new RequestOptions().centerCrop()
                        .error(mFailureImageResouce)
                        .skipMemoryCache(false)
                        .placeholder(mPlaceholderImageResouce)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    public void setImageFilePath(Context context, String path, int ScaleModeModel, int imageWid, int imageHei) {
        Glide.with(context.getApplicationContext()).load(new File("file://" + path)).apply(
                new RequestOptions().centerCrop()
                        .override(imageWid, imageHei)
                        .error(mFailureImageResouce)
                        .skipMemoryCache(false)
                        .placeholder(mPlaceholderImageResouce)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    public void setPlaceholderImageResouce(int resource) {
        this.mPlaceholderImageResouce = resource;
    }

    public void setFailureImage(int resource) {
        this.mFailureImageResouce = resource;
    }

    public void setImageUrlAsGift(Context context, String url) {
        Glide.with(context.getApplicationContext()).asGif().load(url).apply(new RequestOptions().error(
                mFailureImageResouce).skipMemoryCache(false).diskCacheStrategy(DiskCacheStrategy.RESOURCE)).into(this);
    }

    public void setImageByteWithPlaceholder(Context context, byte[] bytes, int ScaleModeModel, int imageWid,
                                            int imageHei) {
        Glide.with(context.getApplicationContext())
                .load(bytes)
                .apply(new RequestOptions().override(imageWid, imageHei)
                               .error(mFailureImageResouce)
                               .skipMemoryCache(false)
                               .placeholder(mPlaceholderImageResouce)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    public void setImageByte(Context context, byte[] bytes, KLMImageView imageView) {
        Glide.with(context.getApplicationContext())
                .asBitmap()
                .load(bytes)
                .apply(new RequestOptions().centerCrop()
                               .error(mFailureImageResouce)
                               .skipMemoryCache(false)
                               .placeholder(mPlaceholderImageResouce)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(
                                context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }
}
