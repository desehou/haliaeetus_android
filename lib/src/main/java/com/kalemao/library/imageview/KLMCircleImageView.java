package com.kalemao.library.imageview;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.kalemao.library.R;
import com.kalemao.library.base.RunTimeData;
import java.io.File;

/**
 * 通用的圆圈图片加载ImageView
 * Created by dim on 2017/5/16 15:40 邮箱：271756926@qq.com
 */
public class KLMCircleImageView extends KLMImageView {

    public KLMCircleImageView(Context context) {
        super(context);
    }

    public KLMCircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KLMCircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void initFailureImage() {
        mFailureImageResouce = R.mipmap.img_head_failed;
    }

    @Override
    protected void initPlaceholderImage() {
        mPlaceholderImageResouce = R.mipmap.img_head_failed;
    }

    @Override
    public void setImageUrl(String url) {
        setImageUrl(RunTimeData.getInstance().getmContext(), url);
    }

    @Override
    public void setImageUrl(Context context, String url) {
        Glide.with(context).load(url).apply(new RequestOptions().centerCrop()
                                                    .circleCrop()
                                                    .error(mFailureImageResouce)
                                                    .placeholder(mPlaceholderImageResouce)
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    @Override
    public void setImageUrl(Context context, String url, int imageWid, int imageHei) {
        Glide.with(context)
                .load(url)
                .apply(new RequestOptions().centerCrop()
                               .override(imageWid, imageHei)
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    @Override
    public void setImageUrl(int res) {
        setImageUrl(RunTimeData.getInstance().getmContext(), res);
    }

    @Override
    public void setImageUrl(Context context, int res) {
        Glide.with(context).load(res).apply(new RequestOptions().centerCrop()
                                                    .circleCrop()
                                                    .error(mFailureImageResouce)
                                                    .placeholder(mPlaceholderImageResouce)
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    @Override
    public void setImageUrl(Context context, int res, int imageWid, int imageHei) {
        Glide.with(context).load(res).apply(new RequestOptions().centerCrop()
                                                    .circleCrop()
                                                    .override(imageWid, imageHei)
                                                    .error(mFailureImageResouce)
                                                    .placeholder(mPlaceholderImageResouce)
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    @Override
    public void setImageFilePath(String path) {
        setImageFilePath(RunTimeData.getInstance().getmContext(), path);
    }

    @Override
    public void setImageFilePath(Context context, String path) {
        Glide.with(context)
                .load(new File("file://" + path))
                .apply(new RequestOptions().centerCrop()
                               .circleCrop()
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    @Override
    public void setImageFilePath(Context context, String path, int ScaleModeModel, int imageWid, int imageHei) {
        Glide.with(context)
                .load(new File("file://" + path))
                .apply(new RequestOptions().centerCrop()
                               .circleCrop()
                               .override(imageWid, imageHei)
                               .error(mFailureImageResouce)
                               .placeholder(mPlaceholderImageResouce)
                               .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(this);
    }

    @Override
    public void setImageUrlAsGift(Context context, String url) {
        Glide.with(context).asGif().load(url).apply(new RequestOptions().circleCrop()
                                                            .error(mFailureImageResouce)
                                                            .placeholder(mPlaceholderImageResouce)
                                                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    @Override
    public void setImageByteWithPlaceholder(Context context, byte[] bytes, int ScaleModeModel, int imageWid,
                                            int imageHei) {
        Glide.with(context).load(bytes).apply(new RequestOptions().override(imageWid, imageHei)
                                                      .error(mFailureImageResouce)
                                                      .placeholder(mPlaceholderImageResouce)
                                                      .circleCrop()
                                                      .diskCacheStrategy(DiskCacheStrategy.ALL)).into(this);
    }

    @Override
    public void setImageByte(Context context, byte[] bytes, KLMImageView imageView) {
        Glide.with(context).asBitmap().load(bytes).apply(new RequestOptions().centerCrop()
                                                                 .placeholder(mPlaceholderImageResouce)
                                                                 .circleCrop()
                                                                 .error(mFailureImageResouce)
                                                                 .diskCacheStrategy(DiskCacheStrategy.ALL)).into(
                new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(
                                context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });
    }
}
