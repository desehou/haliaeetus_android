package com.kalemao.library.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 本地缓存的SharePreference类
 *
 * @author dim.
 * @time 2017/11/15 13:57.
 */
public class BaseSharePreferenceUtil {
    /**
     * 存储的文件名
     */
    public static final String SHARE_UTILS = "share_utils";

    /**
     * 个人token
     */
    public static final String USER_TOKEN       = "user_token";
    /**
     * 店铺token
     */
    public static final String SHOP_TOKEN       = "shop_token";
    /**
     * 设备ID
     */
    public static final String DEVICE_ID        = "device_id";
    /**
     * 聊天的id
     */
    public static final String IM_ID            = "im_id";
    /**
     * 聊天的TOKEN
     */
    public static final String IM_TOKEN           = "im_token";
    /**
     *
     */
    public static final String IM_ID_DEVICE     = "im_id_device";
    /**
     *
     */
    public static final String IM_PSD_DEVICE    = "im_psd_device";
    /**
     * 语种
     */
    public static final String LANGUAGE_TYPE    = "language";
    /**
     * 语言中文
     */
    public static final String LANGUAGE_TYPE_CN = "cn";
    /**
     * 语言英文
     */
    public static final String LANGUAGE_TYPE_EN = "en";

    // 买家版用户登录信息
    public static final String USER_INFO_COUNTRY  = "USER_INFO_COUNTRY";
    public static final String USER_INFO_MOBILE   = "USER_INFO_MOBILE";
    public static final String USER_INFO_BINDMOBILE="USER_INFO_BINDMOBILE";
    public static final String USER_INFO_PASSWORD = "USER_INFO_PASSWORD";
    public static final String USER_INFO_FB       = "USER_INFO_FB";
    public static final String USER_INFO_ID       = "USER_INFO_ID";
    public static final String USER_LOGIN_DATE    = "USER_LOGIN_DATE";
    public static final String USER_INDENTITY     = "user_identity";
    public static final String USER_COUNTY_ID="USER_COUNTY_ID";

    protected        SharedPreferences        sp;
    protected        SharedPreferences.Editor editor;
    protected static BaseSharePreferenceUtil  self;

    public static BaseSharePreferenceUtil getInstance(Context context) {
        if (self == null) {
            self = new BaseSharePreferenceUtil(context);
        }
        return self;
    }

    public BaseSharePreferenceUtil() {

    }

    public BaseSharePreferenceUtil(Context context) {
        sp = context.getApplicationContext().getSharedPreferences(SHARE_UTILS, Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    /**
     * zhongdianyoupin 项目
     */
    public static final String USER_ID               = "user_id";
    public static final String nickname              = "nickname";
    public static final String binding_nengren_token = "binding_nengren_token";
    public static final String created_at            = "created_at";
    public static final String binding_mobile="binding_mobile";
    public static final String NotificationToggle = "NotificationToggle";

    public  String getNotificationToggle() {
        return sp.getString(NotificationToggle, "true");
    }

    public void setNotificationToggle(String notificationToggle) {
        editor.putString(NotificationToggle, notificationToggle);
        editor.commit();
    }


    public void setUserId(String id) {
        editor.putString(USER_ID, id);
        editor.commit();
    }

    public String getUserId() {
        return sp.getString(USER_ID, "");
    }

    public void setNickname(String nick_name) {
        editor.putString(nickname, nick_name);
        editor.commit();
    }

    public String getNickname() {
        return sp.getString(nickname, "");
    }

    public void setNengRenToken(String nengRenToken) {
        editor.putString(binding_nengren_token, nengRenToken);
        editor.commit();
    }

    public String getNengRenToken() {
        return sp.getString(binding_nengren_token, "");
    }

    public void setCreateTime(String createdat) {
        editor.putString(created_at, createdat);
        editor.commit();
    }

    public String getCreatedTime() {
        return sp.getString(created_at, "");
    }

    public void setMobile(String mobile) {
        editor.putString(USER_INFO_MOBILE, mobile);
        editor.commit();
    }

    public String getMobile() {
        return sp.getString(USER_INFO_MOBILE, "");
    }

    public void setBindMobile(String mobile) {
        editor.putString(USER_INFO_BINDMOBILE, mobile);
        editor.commit();
    }

    public String getCountyId() {
        return sp.getString(USER_COUNTY_ID, "");
    }

    public void setCountyId(String CountyId) {
        editor.putString(USER_COUNTY_ID, CountyId);
        editor.commit();
    }

    public String getBindMobile() {
        return sp.getString(USER_INFO_BINDMOBILE, "");
    }

    public void setCountry(String Country) {
        editor.putString(USER_INFO_COUNTRY, Country);
        editor.commit();
    }

    public String getCountry() {
        return sp.getString(USER_INFO_COUNTRY, "");
    }

    public void setPassWord(String passWord) {
        editor.putString(USER_INFO_PASSWORD, passWord);
        editor.commit();
    }

    public String getPassWord() {
        return sp.getString(USER_INFO_PASSWORD, "");
    }

    public void setLoginDate(String loginDate) {
        editor.putString(USER_LOGIN_DATE, loginDate);
        editor.commit();
    }

    public String getLoginDate() {
        return sp.getString(USER_LOGIN_DATE, "");
    }

    public void setUserIdentity(int identity) {
        editor.putInt(USER_INDENTITY, identity);
        editor.commit();
    }

    public int getUserIdentity() {
        return sp.getInt(USER_INDENTITY, 0);
    }

    public void setCurrentLanguage(String language) {
        editor.putString(LANGUAGE_TYPE, language);
        editor.commit();
    }

    public String getCurrentLanguage() {
        return sp.getString(LANGUAGE_TYPE, LANGUAGE_TYPE_EN);
    }

    /**
     * 当前语言是否是中文
     *
     * @author dim.
     * @time 2017/12/14 10:46.
     */
    public boolean doesLanguageCN() {
        if (getCurrentLanguage().equals(LANGUAGE_TYPE_CN)) {
            return true;
        }
        return false;
    }

    public String getUserToken() {
        return sp.getString(USER_TOKEN, "");
    }

    public void setUserToken(String token) {
        editor.putString(USER_TOKEN, token);
        editor.commit();
    }

    public String getDeviceID() {
        return sp.getString(DEVICE_ID, "");
    }

    public void setDeviceID(String device_id) {
        editor.putString(DEVICE_ID, device_id);
        editor.commit();
    }

    public String getIMID() {
        return sp.getString(IM_ID, "");
    }

    public void setIM_ID(String im_id) {
        editor.putString(IM_ID, im_id);
        editor.commit();
    }

    public String getIM_TOKEN() {
        return sp.getString(IM_TOKEN, "");
    }

    public void setIM_TOKEN(String im_psd) {
        editor.putString(IM_TOKEN, im_psd);
        editor.commit();
    }

    public String getIMID_DEVICE() {
        return sp.getString(IM_ID_DEVICE, "");
    }

    public void setIM_ID_DEVICE(String im_id) {
        editor.putString(IM_ID_DEVICE, im_id);
        editor.commit();
    }

    public String getIM_PSD_DEVICE() {
        return sp.getString(IM_PSD_DEVICE, "");
    }

    public void setIM_PSD_DEVICE(String im_psd) {
        editor.putString(IM_PSD_DEVICE, im_psd);
        editor.commit();
    }

    public String getSHOP_TOKEN() {
        return sp.getString(SHOP_TOKEN, "");
    }

    public void setSHOP_TOKEN(String shopToken) {
        editor.putString(SHOP_TOKEN, shopToken);
        editor.commit();
    }
}
