package com.kalemao.library.utils;

import android.os.Environment;

/**
 * 常量基类
 *
 * @author dim.
 * @time 2017/11/15 10:39.
 */
public class BaseComConst {
    /**
     * 系统消息
     */
    public static final String SYSTEM_MSG_CONVERSATION         = "system-54b53072540eeeb8f8e9343e71f28176";
    public static final String SYSTEM_MSG="SystemMsg";
    /**
     * 订单消息
     */
    public static final String ORDER_MSG_CONVERSATION          = "order-70a17ffa722a3985b86d30b034ad06d7";
    public static final String ORDER_MSG="OrderMsg";
    /**
     * 活动资讯消息
     */
    public static final String ACTIVITY_MSG_CONVERSATION       = "activity-69a256025f66e4ce5d15c9dd7225d357";
    public static final String ACTIVITY_MSG="ActivityMsg";

    /**
     * 我的资产
     */
    public static final String ASSET_MSG="AssetMsg";
    /**
     * 返利消息
     */
    public static final String ASSET_MSG_CONVERSATION          = "asset-c04e34d445e31a2159c1bfeb882ba212";
    /**
     * 团队消息
     */
    public static final String TEAM_MSG_CONVERSATION           = "team-f894427cc1c571f79da49605ef8b112f";
    public static final String TEAM_MSG="TeamMsg";
    /**
     * 售后消息
     */
    public static final String AFTERSALE_MSG_CONVERSATION      = "aftersale-2659af2839f42746d43ea25543db3744";
    public static final String AFTER_SALE_MSG="AfterSaleMsg";

    /**
     * 给我的赞
     */
    public static final String LikeMessage="LikeMsg";
    /**
     * 默认的存储图片地址
     */
    public final static String STORE_PATH                      =
            Environment.getExternalStorageDirectory().getAbsolutePath() + "/klm/images/";
    /**
     * 时间格式分隔符：-
     */
    public static final String DATE_SEPARATOR                  = "-";
    /**
     * 时间格式：yyyy-MM-dd
     */
    public static final String DATE_FORMAT                     = "yyyy-MM-dd";
    /**
     * 时间格式：MM-dd
     */
    public static final String DATE_FORMAT_NOYEAR              = "MM-dd";
    /**
     * 时间格式：yyyy-MM-dd HH:mm:ss
     */
    public static final String DATE_TIME_FORMAT                = "yyyy-MM-dd HH:mm:ss";
    /**
     * 时间格式：yyyy-MM-dd HH:mm
     */
    public static final String DATE_TIME_NOSECOND_FORMAT       = "yyyy-MM-dd HH:mm";
    /**
     * 时间格式：yyyy.MM.dd HH:mm
     */
    public static final String DATE_TIME_NOSECOND_FORMAT_POINT = "yyyy.MM.dd HH:mm";
    /**
     * 时间格式：yyyy/MM
     */
    public static final String DATE_FORMAT_MOUNTH              = "yyyy/MM";
    /**
     * 时间格式：yyyy-MM
     */
    public static final String DATE_FORMAT_YEAR_MOUNTH         = "yyyy-MM";

    /**
     * 时间格式：yyyyMM
     */
    public static final String DATE_FORMAT_YEAR_MOUNTH_J         = "yyyyMM";

    /**
     * 时间格式：yyyy
     */
    public static final String DATE_FORMAT_YEAR = "yyyy";

    /**
     * 时间格式：yyyy-MM
     */
    public static final String DATE_FORMAT_YEAR_MOUNTH_CHINESE        = "yyyy年MM月";
    /**
     * 时间格式：MM-dd HH:mm
     */
    public static final String DATE_TIME_NOYEAR_SECOND_FORMAT         = "MM-dd HH:mm";
    /**
     * 时间格式：MM-dd HH:mm:ss
     */
    public static final String DATE_TIME_NOYEAR_FORMAT                = "MM-dd HH:mm:ss";
    /**
     * 时间格式：yyyy-MM-dd EEEE
     */
    public static final String DATE_WEEK_FORMAT                       = "yyyy-MM-dd EEEE";
    /**
     * 时间格式：MM-dd EEEE
     */
    public static final String DATE_WEEK_FORMAT_NO_YEAR               = "MM-dd EEEE";
    /**
     * 时间格式：:
     */
    public static final String TIME_SEPARATOR                         = ":";
    /**
     * 时间格式：HH:mm
     */
    public static final String TIME_FORMAT                            = "HH:mm";
    /**
     * 时间格式：yyyy-MM-dd HH:00
     */
    public static final String DATE_HOUR_FORMAT                       = "yyyy-MM-dd HH:00";
    /**
     * 时间格式：EEEE
     */
    public static final String DATE_WEEK_FORMAT_ONLY                  = "EEEE";
    /**
     * 时间格式：yyyy年MM月dd日 HH:mm
     */
    public static final String DATE_TIME_NOSECOND_FORMAT_CHINA        = "yyyy年MM月dd日 HH:mm";
    /**
     * 时间格式：MM月dd日HH:mm
     */
    public static final String DATE_TIME_NOSECOND_NOYEAR_FORMAT_CHINA = "MM月dd日HH:mm";
    /**
     * 时间格式：yyyyMMddHHmmss
     */
    public static final String DATE_TIME_NOSECOND_FORMAT_FOR_NAME     = "yyyyMMddHHmmss";
    /**
     * 通用常量：BACK_FOR_LOG_NAME
     */
    public static final String BACK_FOR_LOG_NAME                      = "BACK_FOR_LOG_NAME";
    /**
     * 通用常量：10086
     */
    public static final int    RESULT_PICTURE_BACK                    = 10086;

    /**
     * 卡乐猫跳转TAB INDE；
     */
    public static final String KLM_PAGE_INDEX = "KLM_PAGE_INDEX";
}
