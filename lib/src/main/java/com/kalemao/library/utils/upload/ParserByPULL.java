package com.kalemao.library.utils.upload;

import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * xml的解析
 * 
 * @author 张舟俊
 *
 *         2015年10月14日上午11:26:51
 */
public class ParserByPULL {
    // 采用XmlPullParser来解析XML文件
    public static PostResponse getResponse(InputStream inStream) throws Throwable {
        PostResponse response = null;

        // ========创建XmlPullParser,有两种方式=======
        // 方式一:使用工厂类XmlPullParserFactory
        XmlPullParserFactory pullFactory = XmlPullParserFactory.newInstance();
        XmlPullParser parser = pullFactory.newPullParser();
        // 方式二:使用Android提供的实用工具类android.util.Xml
        // XmlPullParser parser = Xml.newPullParser();

        // 解析文件输入流
        parser.setInput(inStream, "UTF-8");
        // 产生第一个事件
        int eventType = parser.getEventType();
        // 只要不是文档结束事件，就一直循环
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
            // 触发开始文档事件
            case XmlPullParser.START_DOCUMENT:

                break;
            // 触发开始元素事件
            case XmlPullParser.START_TAG:
                // 获取解析器当前指向的元素的名称
                String name = parser.getName();
                if ("PostResponse".equals(name)) {
                    // 通过解析器获取id的元素值，并设置student的id
                    response = new PostResponse();
                }
                if (response != null) {
                    if ("Bucket".equals(name)) {
                        // 获取解析器当前指向元素的下一个文本节点的值
                        response.setBucket(parser.nextText());
                    }
                    if ("Location".equals(name)) {
                        // 获取解析器当前指向元素的下一个文本节点的值
                        response.setLocation(parser.nextText());
                    }
                    if ("Key".equals(name)) {
                        // 获取解析器当前指向元素的下一个文本节点的值
                        response.setKey(parser.nextText());
                    }
                    if ("ETag".equals(name)) {
                        response.setETag(parser.nextText());
                    }
                }
                break;
            // 触发结束元素事件
            case XmlPullParser.END_TAG:
                //
                if ("PostResponse".equals(parser.getName())) {
                    
                }
                break;
            default:
                break;
            }
            eventType = parser.next();
        }
        return response;
    }
}
