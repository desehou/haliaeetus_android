package com.kalemao.library.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.widget.Toast;
import java.util.Stack;

/**
 * Activity管理类
 *
 * @author dim.
 * @time 2017/11/15 10:29.
 */
public class ActivityManager {
    protected static ActivityManager mInstance;

    /**
     * 所有的activity集合
     */
    protected Stack<Activity> mActivities = new Stack<Activity>();

    protected ActivityManager() {

    }

    /**
     * 获取当前的存在的Activity的数量
     *
     * @author dim.
     * @time 2017/11/15 10:30.
     */
    public int getActivityCount() {
        return mActivities == null ? 0 : mActivities.size();
    }

    /**
     * 获取ActivityManager的实例
     *
     * @author dim.
     * @time 2017/11/15 10:30.
     */
    public static ActivityManager getInstance() {
        if (mInstance == null) {
            mInstance = new ActivityManager();
        }

        return mInstance;
    }

    /**
     * 增加一个Activity
     *
     * @author dim.
     * @time 2017/11/15 10:32.
     */
    public void addActivity(Activity activity) {
        mActivities.add(activity);
    }

    /**
     * 异常的时候的退出APP，有错误信息toast
     *
     * @author dim.
     * @time 2017/11/15 10:34.
     */
    public void exitApp(final Context context) {
        for (Activity activity : mActivities) {
            if (activity != null) {
                activity.finish();
            }
        }
        mActivities.clear();
        // 使用Toast来显示异常信息

        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(context, "很抱歉,程序出现异常,即将退出。", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }
        }.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    /**
     * 常规的退出APP
     *
     * @author dim.
     * @time 2017/11/15 10:35.
     */
    public void exitAppByNormal(final Context context) {
        for (Activity activity : mActivities) {
            if (activity != null) {
                activity.finish();
            }
        }
        mActivities.clear();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    /**
     * 关闭所有的Activity
     *
     * @author dim.
     * @time 2017/11/15 10:35.
     */
    public void finishAllActivity() {
        if (mActivities != null) {
            while (mActivities.size() > 0) {
                Activity activity = getLastActivity();
                if (activity == null) break;
                popOneActivity(activity);
            }
        }
    }

    /**
     * 关闭到只剩一个Activity
     *
     * @author dim.
     * @time 2017/11/15 10:35.
     */
    public void finishAllActivityNotSelf() {
        if (mActivities != null) {
            while (mActivities.size() > 1) {
                Activity activity = getLastActivity();
                if (activity == null) break;
                popOneActivity(activity);
            }
        }
    }

    /**
     * 关闭一个Activity
     *
     * @author dim.
     * @time 2017/11/15 10:36.
     */
    public void popOneActivity(Activity activity) {
        if (mActivities != null && mActivities.size() > 0) {
            if (activity != null) {
                activity.finish();
                mActivities.remove(activity);
                activity = null;
            }
        }
    }

    /**
     * 获取栈顶的activity，先进后出原则
     *
     * @author dim.
     * @time 2017/11/15 10:36.
     */
    public Activity getLastActivity() {
        if (mActivities == null || mActivities.size() == 0) {
            return null;
        }
        return mActivities.lastElement();
    }
}
