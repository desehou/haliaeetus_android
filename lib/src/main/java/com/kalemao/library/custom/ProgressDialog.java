package com.kalemao.library.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kalemao.library.R;
import com.kalemao.library.utils.ActivityManager;
import com.kalemao.library.utils.BaseComFunc;

public class ProgressDialog extends Dialog {

    private Handler    handler = new Handler();
    private MyRunnable myRun   = new MyRunnable();
    private LinearLayout               show_layer;
    private RelativeLayout             base_layer;
    private WindowManager.LayoutParams lp;
    private TextView                   txtContent;

    public ProgressDialog(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        // super(context, R.style.CustomDialog);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //                     WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //BaseComFunc.setWindowStatusBarColor(ActivityManager.getInstance().getLastActivity(), R.color.zdyp_com_bg);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.layout_progress_dialog);


        lp = this.getWindow().getAttributes();
        lp.dimAmount = 0f;
        show_layer = (LinearLayout) findViewById(R.id.show_layer);
        base_layer = (RelativeLayout) findViewById(R.id.base_layer);
        txtContent = (TextView) findViewById(R.id.progress_txtContent);

        setCancelable(true);
        txtContent.setVisibility(View.GONE);
    }

    public void setMessage(String msg) {
        txtContent.setText(msg);

        txtContent.setVisibility(View.GONE);
    }

    @Override
    public void show() {
        super.show();
    }

    /**
     * 延时消失，因Activity finish而导致崩溃
     */
    public void showProgress() {
        try {
            show_layer.setVisibility(View.INVISIBLE);
            show();
            handler.postDelayed(myRun, 500); // 500毫秒以内响应的请求，不需要出现菊花
        } catch (Exception e) {
            handler.removeCallbacks(myRun);
            if (isShowing()) {
                super.dismiss();
            }
            base_layer.setBackgroundColor(Color.argb(0, 255, 255, 255));
        }
    }

    public void showProgressRightNow() {
        show();
        show_layer.setVisibility(View.VISIBLE);
        base_layer.setBackgroundColor(Color.argb(46, 0, 0, 0));
    }

    @Override
    public void dismiss() {
        try {
            // Log.i("iii","关闭遮盖层");
            handler.removeCallbacks(myRun);
            if (isShowing()) {
                super.dismiss();
            }
            base_layer.setBackgroundColor(Color.argb(0, 255, 255, 255));
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private class MyRunnable implements Runnable {
        @Override
        public void run() {
            try {
                // show();
                show_layer.setVisibility(View.VISIBLE);
                base_layer.setBackgroundColor(Color.argb(46, 0, 0, 0));
            } catch (Exception e) {
            }
        }
    }
}