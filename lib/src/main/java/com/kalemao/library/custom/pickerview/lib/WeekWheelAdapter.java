package com.kalemao.library.custom.pickerview.lib;

/**
 * Numeric Wheel adapter.
 */
public class WeekWheelAdapter implements WheelAdapter {

	/**
	 * Default constructor
	 */
	public WeekWheelAdapter() {
	}

	@Override
	public String getItem(int index) {
		if (index >= 0 && index < getItemsCount()) {
			return weekArray[index];
		}
		return null;
	}

	@Override
	public int getItemsCount() {
		return weekArray.length;
	}

	@Override
	public int getMaximumLength() {
		return weekArray.length;
	}

	private String[] weekArray = { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
}
