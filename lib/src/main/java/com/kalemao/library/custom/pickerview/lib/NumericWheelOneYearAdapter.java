package com.kalemao.library.custom.pickerview.lib;

/**
 * 可以选择全年
 * 
 * @author zhangzhoujun
 *
 *         2015-8-7
 */
public class NumericWheelOneYearAdapter implements WheelAdapter {

	/**
	 * Default constructor
	 */
	public NumericWheelOneYearAdapter() {
		super();
	}

	@Override
	public String getItem(int index) {
		if (index < getItemsCount()) {
			return showItems[index];
		}
		return null;
	}

	@Override
	public int getItemsCount() {
		return showItems.length;
	}

	@Override
	public int getMaximumLength() {
		return showItems.length;
	}

	private String[] showItems = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
}
