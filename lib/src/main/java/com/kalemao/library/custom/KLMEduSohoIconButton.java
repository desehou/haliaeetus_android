package com.kalemao.library.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;

import com.kalemao.library.utils.PackageUtil;

/**
 * android中使用iconfont
 *
 */
public class KLMEduSohoIconButton extends AppCompatButton {

    private Context mContext;

    public KLMEduSohoIconButton(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public KLMEduSohoIconButton(Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        if (PackageUtil.doesMMForMYApp(context)) {
            Typeface iconfont = Typeface.createFromAsset(mContext.getAssets(), "iconfont.ttf");
            setTypeface(iconfont);
        } else if (PackageUtil.doesKLMForMYApp(context)) {
            Typeface iconfont = Typeface.createFromAsset(mContext.getAssets(), "klm.ttf");
            setTypeface(iconfont);
        } else if (PackageUtil.doesAppFoyZDYP(context)) {
            Typeface iconfont = Typeface.createFromAsset(mContext.getAssets(), "zdyp.ttf");
            setTypeface(iconfont);
        }
    }
}
