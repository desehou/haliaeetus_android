package com.kalemao.library.custom.photopicker.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.kalemao.library.R;
import com.kalemao.library.base.BaseActivity;
import com.kalemao.library.custom.photopicker.adapter.ImageBucketAdapter;
import com.kalemao.library.custom.photopicker.model.ImageBucket;
import com.kalemao.library.custom.photopicker.util.CustomConstants;
import com.kalemao.library.custom.photopicker.util.ImageFetcher;
import com.kalemao.library.custom.photopicker.util.IntentConstants;
import com.kalemao.library.utils.BaseComConst;
import com.umeng.analytics.MobclickAgent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import org.jetbrains.annotations.Nullable;

/**
 * 选择相册
 * 
 */

public class ImageBucketChooseActivity extends BaseActivity {
    private ImageFetcher mHelper;
    private List<ImageBucket>  mDataList = new ArrayList<ImageBucket>();
    private ListView           mListView;
    private ImageBucketAdapter mAdapter;
    private int                availableSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHelper = ImageFetcher.getInstance(getApplicationContext());
        initDataBack();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private void initDataBack() {
        mDataList = mHelper.getImagesBucketList(true);
        availableSize = getIntent().getIntExtra(IntentConstants.EXTRA_CAN_ADD_IMAGE_SIZE, CustomConstants.MAX_IMAGE_SIZE);
    }

    private void initView() {
        mListView = (ListView) findViewById(R.id.listview);
        mAdapter = new ImageBucketAdapter(this, mDataList);
        mListView.setAdapter(mAdapter);
        TextView titleTv = (TextView) findViewById(R.id.title);
        titleTv.setText(getResources().getString(R.string.zoom_xiangce));
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectOne(position);

                Intent intent = new Intent(ImageBucketChooseActivity.this, ImageChooseActivity.class);
                intent.putExtra(IntentConstants.EXTRA_IMAGE_LIST, (Serializable) mDataList.get(position).imageList);
                intent.putExtra(IntentConstants.EXTRA_BUCKET_NAME, mDataList.get(position).bucketName);
                intent.putExtra(IntentConstants.EXTRA_CAN_ADD_IMAGE_SIZE, availableSize);

                startActivityForResult(intent, BaseComConst.RESULT_PICTURE_BACK);
                ImageBucketChooseActivity.this.finish();
            }
        });

        TextView cancelTv = (TextView) findViewById(R.id.action);
        cancelTv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ImageBucketChooseActivity.this.finish();
            }
        });
    }

    private void selectOne(int position) {
        int size = mDataList.size();
        for (int i = 0; i != size; i++) {
            if (i == position)
                mDataList.get(i).selected = true;
            else {
                mDataList.get(i).selected = false;
            }
        }
        mAdapter.notifyDataSetChanged();
    }


    @Override
    protected int getContentViewLayoutID() {
        return R.layout.act_image_bucket_choose;
    }

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {

    }
}
