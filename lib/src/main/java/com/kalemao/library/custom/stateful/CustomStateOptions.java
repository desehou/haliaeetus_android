package com.kalemao.library.custom.stateful;

import android.view.View;
import java.io.Serializable;

@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class CustomStateOptions implements Serializable {

    private int                  imageRes;
    private boolean              isLoading;
    private String               message;
    private String               messageEx;
    private String               buttonText;
    private View.OnClickListener buttonClickListener;

    public CustomStateOptions image(int val) {
        imageRes = val;
        return this;
    }

    public CustomStateOptions loading() {
        isLoading = true;
        return this;
    }

    public CustomStateOptions message(String val) {
        message = val;
        return this;
    }

    public CustomStateOptions messageEx(String valEx) {
        messageEx = valEx;
        return this;
    }

    public CustomStateOptions buttonText(String val) {
        buttonText = val;
        return this;
    }

    public CustomStateOptions buttonClickListener(View.OnClickListener val) {
        buttonClickListener = val;
        return this;
    }

    public int getImageRes() {
        return imageRes;
    }



    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageEx() {
        return messageEx;
    }

    public String getButtonText() {
        return buttonText;
    }

    public View.OnClickListener getClickListener() {
        return buttonClickListener;
    }
}
