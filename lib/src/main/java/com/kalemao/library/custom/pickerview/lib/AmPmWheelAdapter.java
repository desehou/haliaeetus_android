package com.kalemao.library.custom.pickerview.lib;

/**
 * 上下午
 * 
 * @author 张舟俊
 *
 *         2016年3月2日下午2:55:33
 */
public class AmPmWheelAdapter implements WheelAdapter {

    /**
     * Default constructor
     */
    public AmPmWheelAdapter() {
    }

    @Override
    public String getItem(int index) {
        if (index >= 0 && index < getItemsCount()) {
            return weekArray[index];
        }
        return null;
    }

    @Override
    public int getItemsCount() {
        return weekArray.length;
    }

    @Override
    public int getMaximumLength() {
        return weekArray.length;
    }

    private String[] weekArray = { "上午", "下午", "", "" };
}
