package com.kalemao.library.custom.font;

import com.kalemao.library.R;
import com.kalemao.library.base.RunTimeData;
import com.kalemao.library.utils.PackageUtil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;


/**
 * 带字体的Button，通过R.styleable.Front设置type，1为fonts/moren_opensans.ttf，2为fonts/avenirnextdemibold.otf
 *
 * @author dim.
 * @time 2017/11/15 11:11.
 */
public class FontButton extends AppCompatButton {

    protected Context mContext;
    protected int     type = 1;

    public FontButton(Context context) {
        super(context);
        mContext = context;
        initView(context, null);
    }

    public FontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.Front);
            type = ta.getInteger(R.styleable.Front_frontType, 1);
        }
        Typeface iconfont = null;
        if (type == 1) {
            if (RunTimeData.getInstance().getTextTypeNormal() == null) {
                RunTimeData.getInstance().setTextTypeNormal(Typeface.createFromAsset(context.getAssets(), "fonts/moren_opensans.ttf"));
            }
            iconfont = RunTimeData.getInstance().getTextTypeNormal();
        } else if (PackageUtil.doesKLMForMYApp(context)) {
            if (RunTimeData.getInstance().getTextTypeBlood() == null) {
                RunTimeData.getInstance().setTextTypeBlood(Typeface.createFromAsset(context.getAssets(), "fonts/avenirnextdemibold.otf"));
            }
            iconfont = RunTimeData.getInstance().getTextTypeBlood();
        }
        setTypeface(iconfont);
    }

}
