package com.kalemao.library.loadingview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class LoadingView extends ImageView {
    private LoadingDrawable mLoadingDrawable;
    private LoadingRenderer loadingRenderer;

    public LoadingView(Context context) {
        super(context);
        try {
            loadingRenderer = LoadingRendererFactory.createLoadingRenderer(context, 9);
            setLoadingRenderer(loadingRenderer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        try {
            loadingRenderer = LoadingRendererFactory.createLoadingRenderer(context, 9);
            setLoadingRenderer(loadingRenderer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLoadingRenderer(LoadingRenderer loadingRenderer) {
        mLoadingDrawable = new LoadingDrawable(loadingRenderer);
        setImageDrawable(mLoadingDrawable);
    }

    public void setProgress(float progress) {
        loadingRenderer.setProgress(progress);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimation();
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);

        final boolean visible = visibility == VISIBLE && getVisibility() == VISIBLE;
        if (visible) {
            startAnimation();
        } else {
            stopAnimation();
        }
    }

    private void startAnimation() {
        if (mLoadingDrawable != null) {
            mLoadingDrawable.start();
        }
    }

    private void stopAnimation() {
        if (mLoadingDrawable != null) {
            mLoadingDrawable.stop();
        }
    }
}
