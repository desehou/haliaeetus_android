package ishangchao.cn.haliaeetus.utils;

import com.kalemao.library.base.RunTimeData;

/**
 * @author by dim
 * @data 2018/6/14 15:19
 * 邮箱：271756926@qq.com
 */

public class HaliaeetusRuntimeData extends RunTimeData {

    public static HaliaeetusRuntimeData getInstance() {
        if (runTimeData == null) {
            runTimeData = new HaliaeetusRuntimeData();
        }
        return (HaliaeetusRuntimeData) runTimeData;
    }
}
