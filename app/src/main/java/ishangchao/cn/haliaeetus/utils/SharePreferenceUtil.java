package ishangchao.cn.haliaeetus.utils;

import android.content.Context;
import com.kalemao.library.utils.BaseSharePreferenceUtil;

/**
 * @author by dim
 * @data 2018/6/15 10:20
 * 邮箱：271756926@qq.com
 */

public class SharePreferenceUtil extends BaseSharePreferenceUtil {

    public static SharePreferenceUtil getInstance(Context context) {
        if (self == null) {
            self = new SharePreferenceUtil(context);
        }
        return (SharePreferenceUtil) self;
    }

    public SharePreferenceUtil(Context context) {
        super(context);
    }
}
