package ishangchao.cn.haliaeetus.utils

import android.content.Context
import android.text.TextUtils
import com.kalemao.library.base.RunTimeData
import com.kalemao.library.utils.BaseComFunc
import com.kalemao.library.utils.BaseSharePreferenceUtil
import ishangchao.cn.haliaeetus.http.HttpNetWork


/**
 * @author by dim
 * @data 2018/6/13 09:54
 * 邮箱：271756926@qq.com
 */
class User private constructor() {

    init {
        println("This ($this) is a singleton")
    }

    companion object {
        val instance = User()
    }

    /**
     * 用户登录成功
     *
     * @author dim.
     * @time 2018/6/15 13:08.
     */
    fun userLogin(token : String,context : Context){
        SharePreferenceUtil.getInstance(context).userToken = token
        setHead(context)
    }

    /**
     * 用户退出登录
     *
     * @author dim.
     * @time 2018/6/15 13:08.
     */
    fun userLogout(context : Context){
        SharePreferenceUtil.getInstance(context).userToken = ""
        setHead(context)
    }

    /**
     * 设置网络请求的httphead
     *
     * @author dim.
     * @time 2018/6/15 13:08.
     */
    fun setHead(context: Context) {
        val httpHead = HashMap<String, String>()

        httpHead["X-App-Token"] = "d2688d6d6d5e1b4667df8335c057f928"
        httpHead["X-Auth-Token"] = BaseSharePreferenceUtil.getInstance(context).userToken
        httpHead["X-Device-Id"] = BaseComFunc.getUUID(context)
        httpHead["X-App-Type"] = "android"
        httpHead["X-Locale"] = "zn_CH"
        httpHead["X-App-Version"] = BaseComFunc.getAppVersionName(context)
        httpHead["Content-Type"] = "application/json;charset=UTF-8"
        httpHead["Accept"] = "application/json"

        RunTimeData.getInstance().httpHead = httpHead
        HttpNetWork.getInstance().genericClientForLog(true)
        HttpNetWork.getInstance().afterHttpHeadChanged()
    }

    /**
     * 当前用户是否已经登录
     *
     * @author dim.
     * @time 2018/6/15 13:08.
     */
    fun isLogin(context: Context): Boolean {
        if (TextUtils.isEmpty(BaseSharePreferenceUtil.getInstance(context).userToken)) {
            return false
        }
        return true
    }
}