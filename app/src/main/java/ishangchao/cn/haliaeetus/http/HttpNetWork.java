package ishangchao.cn.haliaeetus.http;

import com.kalemao.library.base.MResponse;
import com.kalemao.library.http.BaseNetWork;

/**
 * Created by junlihou on 2018/6/12.
 */

public class HttpNetWork extends BaseNetWork {

    public static HttpNetWork getInstance() {
        if (instance == null) {
            instance = new HttpNetWork();
        }
        return (HttpNetWork) instance;
    }

    public HaliaeetusApi selfApi;

    public HaliaeetusApi getHaliaeetusApi() {
        if (selfApi == null) {
            selfApi = (HaliaeetusApi) getApi(HaliaeetusApi.class, IP_FOR_HALIAEETUS, new MResponse.JsonAdapter());
        }
        return selfApi;
    }

    @Override
    public void afterHttpHeadChanged() {
        okHttpClient = null;
        if (selfApi != null) {
            selfApi = null;
            getHaliaeetusApi();
        }
    }
}
