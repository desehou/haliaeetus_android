package ishangchao.cn.haliaeetus.http

import com.kalemao.library.base.MResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap
import rx.Observable
import java.util.HashMap

/**
 * Created by junlihou on 2018/6/7.
 */
interface HaliaeetusApi {
    // 获取优惠地址
    @GET("seed_codes/generate")
    abstract fun getGenerate(): Observable<MResponse>

    // 创建行程
    @POST("schedules")
    abstract fun createSchedules(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>


    // 验证优惠码
    @POST("schedules/{id}/verify_coupon")
    abstract fun verifyCoupon(@Path(
            "id") strID: String, @QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 确认行程
    @POST("schedules/{id}/confirm")
    abstract fun confirm(@Path(
            "id") strID: String, @QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 设置确认码
    @POST("schedules/{id}/set_confirmation_number")
    abstract fun setConfirmationNumber(@Path(
            "id") strID: String, @QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 发送验证码(仅用于注册)
    @POST("sms/sign_up")
    abstract fun smsSignUp(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 注册账号
    @POST("users")
    abstract fun regiestUser(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 密码登录
    @POST("sessions")
    abstract fun sessionsLogin(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 发送验证码(忘记密码)
    @POST("passwords/send_code")
    abstract fun smsForgetPwd(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 重置密码
    @POST("passwords/reset")
    abstract fun passwordsReset(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 个人详情接口
    @GET("profile")
    abstract fun getUserProfile(): Observable<MResponse>

    // 修改个人信息
    @PUT("profile")
    abstract fun chageProfile(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 新增信用卡
    @POST("credit_cards")
    abstract fun caeditCards(@QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 更新信用卡
    @PUT("credit_cards/{id}")
    abstract fun changeCards(@Path(
            "id") id: String?, @QueryMap hashMap: HashMap<String, String>): Observable<MResponse>

    // 支付历史列表
    @GET("transaction_records")
    abstract fun getTransactionRecords(@Query("page") page: Int, @Query(
            "per_page") per_page: Int): Observable<MResponse>

    // 我的行程列表
    @GET("schedules")
    abstract fun getSchedules(@Query("page") page: Int, @Query(
            "per_page") per_page: Int): Observable<MResponse>

    // 行程详情
    @GET("schedules/{id}")
    abstract fun getSchedulesDetails(@Path("id") id: String?): Observable<MResponse>

    // 版本更新
    @GET("versions/check")
    abstract fun getVersionsCheck(): Observable<MResponse>
}