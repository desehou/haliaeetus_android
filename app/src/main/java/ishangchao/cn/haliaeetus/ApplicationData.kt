package ishangchao.cn.haliaeetus

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.kalemao.library.logutils.LogUtil
import ishangchao.cn.haliaeetus.utils.HaliaeetusRuntimeData

/**
 * @author by dim
 * @data 2018/6/14 15:17
 * 邮箱：271756926@qq.com
 */
class ApplicationData : MultiDexApplication() {

    companion object {
        lateinit var instance: ApplicationData
            private set
    }


    override fun onCreate() {
        super.onCreate()

        instance = this
        HaliaeetusRuntimeData.getInstance().setmContext(this)
        LogUtil.init(true)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}