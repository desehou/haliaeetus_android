package ishangchao.cn.haliaeetus.loading

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.logutils.LogUtil
import com.kalemao.library.utils.ActivityManager
import ishangchao.cn.haliaeetus.MainActivity
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.utils.SharePreferenceUtil
import ishangchao.cn.haliaeetus.utils.User

/**
 * @author by dim
 * @data 2018/6/15 10:15
 * 邮箱：271756926@qq.com
 */
class LoadingActivity : HaliaeetusActivity(), Runnable {
    private var handler: Handler? = null

    override protected fun getContentViewLayoutID(): Int {
        // 去掉Activity上面的状态栏
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        return R.layout.act_loading
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        init()
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
            grantResults: IntArray) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            init()
        } else {
            ActivityManager.getInstance().exitAppByNormal(this)
        }
    }

    private fun gotoMain(deslay: Long) {
        handler = Handler()
        handler!!.postDelayed(this, deslay)
    }

    private fun init() {
        SharePreferenceUtil.getInstance(this)
        if (doesNeedCheckoutPermissionWriteExternalStorage()) {
            LogUtil.d("App", "申请权限  doesNeedCheckoutPermissionWriteExternalStorage")
        } else if (doesNeedCheckoutPermissionPhotoState()) {
            LogUtil.d("App", "申请权限  doesNeedCheckoutPermissionPhotoState")
        } else {
            User.instance.setHead(this)
            gotoMain(3000)
        }
    }

    override fun run() {
        val intent = Intent()
        intent.setClass(this@LoadingActivity, MainActivity::class.java!!)
        this@LoadingActivity.startActivity(intent)
    }
}