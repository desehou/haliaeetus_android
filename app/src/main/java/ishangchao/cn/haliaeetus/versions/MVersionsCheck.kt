package ishangchao.cn.haliaeetus.versions

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/7/5 10:44
 * 邮箱：271756926@qq.com
 */
class MVersionsCheck : Serializable {

    var title = ""
    var version = ""
    var size = ""
    var is_forced = false
    var is_latested = false
    var update_desc = ""
    var download_url = ""

}