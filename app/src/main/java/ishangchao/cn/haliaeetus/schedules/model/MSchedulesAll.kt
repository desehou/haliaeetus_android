package ishangchao.cn.haliaeetus.schedules.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/15 15:53
 * 邮箱：271756926@qq.com
 */
class MSchedulesAll : Serializable {

    var schedules = ArrayList<MSchedulesBean>()
    var current_page = 1
    var total = 1
    var per_page = 1

}