package ishangchao.cn.haliaeetus.schedules

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.widget.RelativeLayout
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.databinding.ViewShouxufeiBinding
import org.jetbrains.anko.find

/**
 * @author by dim
 * @data 2018/7/10 16:41
 * 邮箱：271756926@qq.com
 */
class ShouxufeiView : RelativeLayout {
    private var binding: ViewShouxufeiBinding? = null
    private var mContext: Context? = null
    private var mCommission = ""
    private var mListener: OnSHouxufeiPayListener? = null

    constructor(context: Context, commission: String, listener: OnSHouxufeiPayListener) : super(context) {
        val v = LayoutInflater.from(context).inflate(R.layout.view_shouxufei, this)
        this.mContext = context
        this.mCommission = commission
        this.mListener = listener
        binding = DataBindingUtil.bind(v.find(R.id.view_shouxufei_root_layout))
        initView()
    }

    private fun initView() {
        binding!!.shouxufei = mCommission + "元"
        binding!!.setMyClick {
            when (it.id) {
                R.id.view_shouxufei_pay -> {
                    if (mListener != null) {
                        mListener!!.onShouxufeiClick()
                    }
                }
            }
        }
    }

    fun removeCallBacks() {
        if (mListener != null) {
            mListener = null
        }
    }

    interface OnSHouxufeiPayListener {
        fun onShouxufeiClick()
    }
}