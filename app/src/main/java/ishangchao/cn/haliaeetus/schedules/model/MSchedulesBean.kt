package ishangchao.cn.haliaeetus.schedules.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/15 15:51
 * 邮箱：271756926@qq.com
 */
class MSchedulesBean : Serializable {

    var id = ""
    var check_in_date = ""
    var check_out_date = ""
    var rooms = 0
    var adults = 0
    var minors = 0
    var country = ""
    var city_name = ""
    var place_name = ""
    var longitude = ""
    var latitude = ""
    var confirmation_number = ""
    var extra = ""

}