package ishangchao.cn.haliaeetus.schedules

import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import com.kalemao.library.http.JsonUtils
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActSchedulesRoomBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.schedules.model.MSchedulesConfirm
import ishangchao.cn.haliaeetus.schedules.model.MSchedulesCoupon
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.HashMap

/**
 * @author by dim
 * @data 2018/7/10 13:55
 * 邮箱：271756926@qq.com
 */
class SchedulesRoomActivity : HaliaeetusActivity() {

    lateinit var binding: ActSchedulesRoomBinding
    private var mDetailsID: String? = null
    private var mCoupons: MSchedulesCoupon? = null
    private var mCanClick = false
    private var mCommission = 0
    private var mCouponSn = ""

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_schedules_room
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActSchedulesRoomBinding
        mStatefullLayout = binding.actScRoomStateful

        binding.actScRoomTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.setMyClick {
            when (it.id) {
                R.id.act_sc_room_codet -> {
                    checkCoupon()
                }
                R.id.act_sc_room_agree_layout -> {
                    mCanClick = !mCanClick
                    binding.canClick = mCanClick
                }
                R.id.act_sc_room_continue -> {
                    // 继续下单
                    confirmOrder()
                }
            }
        }
        mDetailsID = intent.getStringExtra("id")
        mCommission = intent.getIntExtra("commission", 100)

        showCouponsData()
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actScRoomStateful
        } else null
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

    private fun checkCoupon() {
        mCouponSn = binding.actScRoomCodeInput.text.toString()
        if (TextUtils.isEmpty(mCouponSn)) {
            toast("请先输入优惠码")
            return
        }
        var hashMap: HashMap<String, String> = HashMap()
        hashMap["coupon_sn"] = mCouponSn
        HttpNetWork.getInstance().haliaeetusApi.verifyCoupon(mDetailsID!!, hashMap).subscribeOn(
                Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
                closeLoading()
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    mCoupons = JsonUtils.fromJsonDate(mResponse.data, MSchedulesCoupon::class.java)
                    binding.couponChecked = true
                    showCouponsData()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    private fun confirmOrder() {
        if (!mCanClick) {
            toast("请先同意条款")
            return
        }
        var hashMap: HashMap<String, String> = HashMap()
        hashMap["coupon_sn"] = mCouponSn
        HttpNetWork.getInstance().haliaeetusApi.confirm(mDetailsID!!, hashMap).subscribeOn(
                Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
                closeLoading()
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    var confirmData = JsonUtils.fromJsonDate(mResponse.data, MSchedulesConfirm::class.java)

                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    private fun showCouponsData() {
        var des = String.format("%s元", mCommission)
        if (mCoupons != null) {
            des = String.format("%s元（优惠券已抵扣%s元）", mCoupons!!.commission, mCoupons!!.coupon_amount)
        }
        var html = "<html>本订单需要额外支付<font color=\"#666666\"></font>" +
                "<font color=\"#666666\">" + "<strong>手续费</strong> </font>" +
                "<font color=\"#FF8623\">" + des + "</font>" +
                "<font color=\"#666666\">" + "，预约一经确认概不退款。(手续费费用为每日100元)</font>" +
                "</html>"

        binding.actScRoomDes.text = Html.fromHtml(html)
    }
}