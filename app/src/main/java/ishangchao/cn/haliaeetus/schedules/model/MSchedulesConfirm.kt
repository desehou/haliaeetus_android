package ishangchao.cn.haliaeetus.schedules.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/7/10 15:07
 * 邮箱：271756926@qq.com
 */
class MSchedulesConfirm : Serializable {

    var schedule_id = ""
    var commission = ""
    var balance = ""
    var order_sn = ""

}