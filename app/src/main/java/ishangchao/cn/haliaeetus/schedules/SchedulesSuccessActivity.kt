package ishangchao.cn.haliaeetus.schedules

import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import com.kalemao.library.custom.popupWindow.CommonPopupWindow
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActSchedulesRoomSuccessBinding
import ishangchao.cn.haliaeetus.schedules.ShouxufeiView.OnSHouxufeiPayListener

/**
 * @author by dim
 * @data 2018/7/10 16:18
 * 邮箱：271756926@qq.com
 */
class SchedulesSuccessActivity : HaliaeetusActivity() {
    lateinit var binding: ActSchedulesRoomSuccessBinding
    private var mDetailsID: String? = null

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActSchedulesRoomSuccessBinding
        mStatefullLayout = binding.actScSuccessStateful
        mDetailsID = intent.getStringExtra("id")

        binding.actScSuccessTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.setMyClick {
            when (it.id) {
                R.id.act_sc_success_detail -> {
                    gotoDetails()
                }
                R.id.act_sc_success_main -> {
                    gotoMain()
                }

            }
        }
    }

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_schedules_room_success
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actScSuccessStateful
        } else null
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

    private fun gotoDetails() {

    }

    private fun gotoMain() {
        var popupWindow: CommonPopupWindow? = null
        var shouxuView = ShouxufeiView(this, "100", object : OnSHouxufeiPayListener {
            override fun onShouxufeiClick() {
                popupWindow!!.dismiss()
            }
        })

        popupWindow = CommonPopupWindow.Builder(this@SchedulesSuccessActivity)
                .setView(shouxuView)
                .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT,
                        resources.getDimension(R.dimen.dp_320).toInt())
                .setAnimationStyle(R.style.AnimUp)
                .setOutsideTouchable(false)
                .setBackGroundLevel(0.6f)
                .create()
        popupWindow!!.setOnDismissListener {
            if (shouxuView != null) {
                shouxuView.removeCallBacks()
            }
            popupWindow = null
        }
        popupWindow!!.showAtLocation(binding.actScSuccessStateful, Gravity.CENTER, 0,
                0)
    }
}