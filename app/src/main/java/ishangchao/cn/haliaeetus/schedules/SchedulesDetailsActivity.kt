package ishangchao.cn.haliaeetus.schedules

import android.os.Bundle
import android.text.TextUtils
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import com.kalemao.library.http.JsonUtils
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActSchedulesDetailBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.login.event.MessageRegiestEvent
import ishangchao.cn.haliaeetus.login.model.MRegiest
import ishangchao.cn.haliaeetus.schedules.model.MSchedulesBean
import ishangchao.cn.haliaeetus.utils.User
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * @author by dim
 * @data 2018/6/15 16:57
 * 邮箱：271756926@qq.com
 */
class SchedulesDetailsActivity : HaliaeetusActivity() {
    lateinit var binding: ActSchedulesDetailBinding
    private var mDetailsID = ""
    private var mDetails : MSchedulesBean ?= null

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_schedules_detail
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActSchedulesDetailBinding
        mStatefullLayout = binding.actScDetailStateful

        binding.actScDetailTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })
        mDetailsID = intent.getStringExtra("id")
        getDetails()
    }

    private fun getDetails() {
        if (TextUtils.isEmpty(mDetailsID)) {
            this.finish()
        }
        HttpNetWork.getInstance().haliaeetusApi.getSchedulesDetails(mDetailsID).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
                closeLoading()
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    mDetails = JsonUtils.fromJsonDate(mResponse.data, MSchedulesBean::class.java)
                    binding.scheduledBean = mDetails
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actScDetailStateful
        } else null
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

}