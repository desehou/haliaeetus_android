package ishangchao.cn.haliaeetus.schedules.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/7/10 14:06
 * 邮箱：271756926@qq.com
 */
class MSchedulesCoupon : Serializable{

    var coupon_amount = 0
    var commission = 0

}