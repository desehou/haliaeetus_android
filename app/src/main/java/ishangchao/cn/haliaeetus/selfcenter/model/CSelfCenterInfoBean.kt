package ishangchao.cn.haliaeetus.selfcenter.model

import android.graphics.drawable.Drawable
import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/14 11:46
 * 邮箱：271756926@qq.com
 */
class CSelfCenterInfoBean(var icon: Drawable, var title: String = "", var des: String = "") : Serializable {

}