package ishangchao.cn.haliaeetus.selfcenter.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/15 14:37
 * 邮箱：271756926@qq.com
 */
class MPayHistory : Serializable {

    var id: Int = -1
    var category: String = ""
    var amount: String = ""
    var balance: String = ""
    var created_at: String = ""

}