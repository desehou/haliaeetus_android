package ishangchao.cn.haliaeetus.selfcenter

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.http.BaseObserver
import com.kalemao.library.http.JsonUtils
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusFragment
import ishangchao.cn.haliaeetus.databinding.SelfCenterFragmentBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.login.LoginActivity
import ishangchao.cn.haliaeetus.schedules.SchedulesSuccessActivity
import ishangchao.cn.haliaeetus.selfcenter.event.MessageCardEvent
import ishangchao.cn.haliaeetus.selfcenter.event.MessageUserInfoEvent
import ishangchao.cn.haliaeetus.selfcenter.model.CSelfCenterInfoBean
import ishangchao.cn.haliaeetus.selfcenter.model.MSelfCenterCardInfo
import ishangchao.cn.haliaeetus.utils.User
import org.jetbrains.anko.support.v4.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by junlihou on 2018/6/6.
 */
class SelfCenterFragment : HaliaeetusFragment() {
    private var mInfo: MSelfCenterCardInfo? = null
    /**
     * 是否是第一次加载
     */
    var first: Boolean = true
    lateinit var mBinding: SelfCenterFragmentBinding

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mBinding.user = null
        setHead()
        if (User.instance.isLogin(activity!!)) {
            logined()
        }
    }

    private fun setHead() {
        if (mInfo == null || mInfo!!.profile == null || TextUtils.isEmpty(mInfo!!.profile!!.avatar_url)) {
            mBinding.fraScHead.setImageUrl(activity, R.mipmap.img_head_default)
        } else {
            mBinding.fraScHead.setImageUrl(activity, mInfo!!.profile!!.avatar_url)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun beforeInit() {
        super.beforeInit()

        doesUseDataBanding = true
    }

    override fun setStatefulLayout(): StatefulLayout? {
        if (!first && mBinding != null) {
            return mBinding.fraScStateful
        }
        return null
    }

    override fun getLayoutId(): Int {
        return R.layout.self_center_fragment
    }

    override fun initData() {

    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        mBinding = mDataBanding as SelfCenterFragmentBinding
        first = false
        mBinding.setMyClick {
            when (it.id) {
                R.id.fra_sc_login -> {
                    onLoginClick()
                }
                R.id.fra_sc_logout -> {
                    onLogoutClick()
                }
            }

        }

        var infoList = ArrayList<CSelfCenterInfoBean>()
        infoList.add(CSelfCenterInfoBean(resources.getDrawable(R.mipmap.img_self_info), "个人信息", "补充完整个人信息将自动填写预定信息"))
        infoList.add(CSelfCenterInfoBean(resources.getDrawable(R.mipmap.img_self_pay), "支付信息", "填写支付信息将自动填写预定信息"))
        infoList.add(CSelfCenterInfoBean(resources.getDrawable(R.mipmap.img_self_history), "账户历史", "最近一个月"))
        mBinding.data = infoList

        mBinding.fraScInfoSelf!!.itemSelfCenterRoot.setOnClickListener { _ -> onSelfInfoClick() }
        mBinding.fraScInfoPay!!.itemSelfCenterRoot.setOnClickListener { _ -> onPayInfoClick() }
        mBinding.fraScInfoHistory!!.itemSelfCenterRoot.setOnClickListener { _ -> onHistoryClick() }

    }

    /**
     * 登录的点击
     *
     * @author dim.
     * @time 2018/6/14 14:20.
     */
    private fun onLoginClick() {
        var intent = Intent()
        intent.setClass(activity, LoginActivity::class.java)
        activity!!.startActivity(intent)
    }

    /**
     * 登出的点击
     *
     * @author dim.
     * @time 2018/6/14 14:20.
     */
    private fun onLogoutClick() {
        User.instance.userLogout(activity!!)
        mInfo = null
        mBinding.user = null
        setHead()
    }

    /**
     * 个人信息的点击
     *
     * @author dim.
     * @time 2018/6/14 13:17.
     */
    private fun onSelfInfoClick() {
//        if (User.instance.isLogin(activity!!) || mInfo == null) {
//            var intent = Intent()
//            intent.setClass(activity, SelfInfoActivity::class.java)
//            intent.putExtra("info", mInfo!!.profile)
//            activity!!.startActivity(intent)
//        } else {
//            onLoginClick()
//        }
        var intent = Intent()
        intent.setClass(activity, SchedulesSuccessActivity::class.java)
        activity!!.startActivity(intent)

    }


    /**
     * 支付信息的点击
     *
     * @author dim.
     * @time 2018/6/14 13:17.
     */
    private fun onPayInfoClick() {
        if (User.instance.isLogin(activity!!) || mInfo == null) {
            var intent = Intent()
            intent.setClass(activity, CardInfoActivity::class.java)
            intent.putExtra("info", mInfo!!.credit_card)
            activity!!.startActivity(intent)
        } else {
            onLoginClick()
        }
    }

    /**
     * 账户历史的点击
     *
     * @author dim.
     * @time 2018/6/14 13:17.
     */
    private fun onHistoryClick() {
        if (User.instance.isLogin(activity!!) || mInfo == null) {
            var intent = Intent()
            intent.setClass(activity, PayHistoryActivity::class.java)
            activity!!.startActivity(intent)
        } else {
            onLoginClick()
        }
    }

    /**
     * 用户登录之后的信息的获取
     *
     * @author dim.
     * @time 2018/6/15 13:10.
     */
    fun logined() {
        HttpNetWork.getInstance().haliaeetusApi.getUserProfile().subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
                closeLoading()
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    mInfo = JsonUtils.fromJsonDate(mResponse.data, MSelfCenterCardInfo::class.java)
                    mBinding.user = mInfo!!.profile
                    setHead()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    /**
     * 用户信息修改之后
     *
     * @author dim.
     * @time 2018/6/15 13:45.
     */
    fun changeUserInfo(info: MessageUserInfoEvent) {
        mInfo!!.profile!!.first_name = info.first_name
        mInfo!!.profile!!.last_name = info.last_name
        mInfo!!.profile!!.email = info.email
        mInfo!!.profile!!.us_phone = info.us_phone
    }

    /**
     * 用户的支付信息修改之后
     *
     * @author dim.
     * @time 2018/6/15 14:11.
     */
    fun changeUserCard(info: MessageCardEvent) {
        if (info.id == -1) {
            logined()
        } else {
            mInfo!!.credit_card!!.card_name = info.card_name
            mInfo!!.credit_card!!.card_number = info.card_number
            mInfo!!.credit_card!!.card_type = info.card_type
            mInfo!!.credit_card!!.expiration_month = info.expiration_month
            mInfo!!.credit_card!!.expiration_year = info.expiration_year
        }
    }

}