package ishangchao.cn.haliaeetus.selfcenter.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/14 11:46
 * 邮箱：271756926@qq.com
 */
class MSelfCenterCard : Serializable {
    var id: Int = -1
    var card_name: String = ""
    var card_type: String = ""
    var card_number: String = ""
    var expiration_year: String = ""
    var expiration_month: String = ""

}