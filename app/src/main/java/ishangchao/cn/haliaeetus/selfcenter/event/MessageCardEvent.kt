package ishangchao.cn.haliaeetus.selfcenter.event

/**
 * @author by dim
 * @data 2018/6/15 11:19
 * 邮箱：271756926@qq.com
 */
class MessageCardEvent(var id: Int,
        var card_name: String,
        var card_type: String,
        var card_number: String,
        var expiration_year: String,
        var expiration_month: String) {

}