package ishangchao.cn.haliaeetus.selfcenter.event

/**
 * @author by dim
 * @data 2018/6/15 11:19
 * 邮箱：271756926@qq.com
 */
class MessageUserInfoEvent(var first_name: String, var last_name: String, var email: String, var us_phone: String) {

}