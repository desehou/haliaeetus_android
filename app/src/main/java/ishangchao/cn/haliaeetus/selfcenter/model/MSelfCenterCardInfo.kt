package ishangchao.cn.haliaeetus.selfcenter.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/14 11:46
 * 邮箱：271756926@qq.com
 */
class MSelfCenterCardInfo : Serializable {
    var profile: MSelfCenterProfile? = null
    var credit_card: MSelfCenterCard? = null

}