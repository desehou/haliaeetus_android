package ishangchao.cn.haliaeetus.selfcenter

import android.os.Bundle
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActSelfInfoBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.selfcenter.event.MessageUserInfoEvent
import ishangchao.cn.haliaeetus.selfcenter.model.MSelfCenterProfile
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.HashMap

/**
 * @author by dim
 * @data 2018/6/15 13:36
 * 邮箱：271756926@qq.com
 */
class SelfInfoActivity : HaliaeetusActivity() {
    lateinit var binding: ActSelfInfoBinding

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_self_info
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActSelfInfoBinding
        mStatefullLayout = binding.actSelfinfoStateful

        var selfInfo = intent.getSerializableExtra("info") as MSelfCenterProfile
        if(selfInfo != null){
            binding.actSelfinfoNameFirst.setText(selfInfo.first_name)
            binding.actSelfinfoNameSecond.setText(selfInfo.last_name)
            binding.actSelfinfoEmail.setText(selfInfo.email)
            binding.actSelfinfoPhone.setText(selfInfo.us_phone)
        }

        binding.actSelfinfoTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.setMyClick {
            when (it.id) {
                R.id.act_selfinfo_save -> {
                    saveInfo()
                }
            }
        }
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actSelfinfoStateful
        } else null
    }

    /**
     * 保存用户信息
     *
     * @author dim.
     * @time 2018/6/15 13:46.
     */
    private fun saveInfo() {
        var first_name = binding.actSelfinfoNameFirst.text.toString()
        var last_name = binding.actSelfinfoNameSecond.text.toString()
        var email = binding.actSelfinfoEmail.text.toString()
        var us_phone = binding.actSelfinfoPhone.text.toString()

        showLoading()
        var hashMap: HashMap<String, String> = HashMap()
        hashMap["first_name"] = first_name
        hashMap["last_name"] = last_name
        hashMap["email"] = email
        hashMap["us_phone"] = us_phone
        HttpNetWork.getInstance().haliaeetusApi.chageProfile(hashMap).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
                closeLoading()
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    EventBus.getDefault().post(MessageUserInfoEvent(first_name, last_name, email, us_phone))
                    toast("修改个人信息成功")
                    this@SelfInfoActivity.finish()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }
}