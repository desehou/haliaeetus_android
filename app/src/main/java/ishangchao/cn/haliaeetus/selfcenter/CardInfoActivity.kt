package ishangchao.cn.haliaeetus.selfcenter

import android.os.Bundle
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActSelfCardBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.selfcenter.event.MessageCardEvent
import ishangchao.cn.haliaeetus.selfcenter.model.MSelfCenterCard
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.HashMap

/**
 * @author by dim
 * @data 2018/6/15 14:07
 * 邮箱：271756926@qq.com
 */
class CardInfoActivity : HaliaeetusActivity() {
    lateinit var binding: ActSelfCardBinding
    private var cartID = -1

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_self_card
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActSelfCardBinding
        mStatefullLayout = binding.actCardStateful

        var cardInfo = intent.getSerializableExtra("info") as MSelfCenterCard
        if (cardInfo != null) {
            cartID = cardInfo.id
            binding.actCardName.setText(cardInfo.card_name)
            binding.actCardNameType.setText(cardInfo.card_type)
            binding.actCardNum.setText(cardInfo.card_number)
            binding.actCardYear.setText(cardInfo.expiration_year)
            binding.actCardMonth.setText(cardInfo.expiration_month)
        }

        binding.actCardTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.setMyClick {
            when (it.id) {
                R.id.act_card_save -> {
                    saveInfo()
                }
            }
        }

    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actCardStateful
        } else null
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

    private fun saveInfo() {
        var card_name = binding.actCardName.text.toString()
        var card_type = binding.actCardNameType.text.toString()
        var card_number = binding.actCardNum.text.toString()
        var expiration_year = binding.actCardYear.text.toString()
        var expiration_month = binding.actCardMonth.text.toString()

        var hashMap: HashMap<String, String> = HashMap()
        hashMap["card_name"] = card_name
        hashMap["card_type"] = card_type
        hashMap["card_number"] = card_number
        hashMap["expiration_year"] = expiration_year
        hashMap["expiration_month"] = expiration_month
        // 新增
        if (cartID == -1) {
            showLoading()
            HttpNetWork.getInstance().haliaeetusApi.caeditCards(hashMap).subscribeOn(Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
                override fun onError(e: Throwable) {
                    super.onError(e)
                    closeLoading()
                }

                override fun onNext(mResponse: MResponse) {
                    if (mResponse.doesSuccess()) {
                        EventBus.getDefault().post(
                                MessageCardEvent(-1, card_name, card_type, card_number, expiration_year,
                                        expiration_month))
                        toast("支付信息保存成功")
                        this@CardInfoActivity.finish()
                    } else {
                        toast(mResponse.biz_msg)
                    }
                    closeLoading()
                }
            })
        } else {
            showLoading()
            HttpNetWork.getInstance().haliaeetusApi.changeCards(cartID.toString(), hashMap).subscribeOn(
                    Schedulers.io()).observeOn(
                    AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
                override fun onError(e: Throwable) {
                    super.onError(e)
                    closeLoading()
                }

                override fun onNext(mResponse: MResponse) {
                    if (mResponse.doesSuccess()) {
                        EventBus.getDefault().post(
                                MessageCardEvent(cartID, card_name, card_type, card_number, expiration_year,
                                        expiration_month))
                        toast("支付信息保存成功")
                        this@CardInfoActivity.finish()
                    } else {
                        toast(mResponse.biz_msg)
                    }
                    closeLoading()
                }
            })
        }
    }
}