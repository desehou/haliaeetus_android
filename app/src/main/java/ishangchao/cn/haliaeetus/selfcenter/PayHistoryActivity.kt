package ishangchao.cn.haliaeetus.selfcenter

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView.ViewHolder
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.recycler_headandfoot.HeaderAndFooterAdapter
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import com.kalemao.library.http.JsonUtils
import com.kalemao.library.refresh.SuperSwipeRefreshLayout
import ishangchao.cn.haliaeetus.BR
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.CommonItemViewHolder
import ishangchao.cn.haliaeetus.base.CommonRecyclerViewAdapter
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActPayHistoryBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.selfcenter.model.MPayHistory
import ishangchao.cn.haliaeetus.selfcenter.model.MPayHistoryAll
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * @author by dim
 * @data 2018/6/15 14:45
 * 邮箱：271756926@qq.com
 */
class PayHistoryActivity : HaliaeetusActivity(), SuperSwipeRefreshLayout.OnPullRefreshListener, SuperSwipeRefreshLayout.OnPushLoadMoreListener {
    lateinit var binding: ActPayHistoryBinding
    private var mHistory: MPayHistoryAll? = null

    private var mPayHistory = ArrayList<MPayHistory>()
    private var mAdapter: CommonRecyclerViewAdapter<MPayHistory>? = null
    lateinit var mHeader: HeaderAndFooterAdapter<ViewHolder>

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_pay_history
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActPayHistoryBinding
        mStatefullLayout = binding.actPayhistoryStateful

        binding.actPayhistoryTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.actPayhistoryRefresh.setOnPushLoadMoreListener(this)
        binding.actPayhistoryRefresh.setOnPullRefreshListener(this)
        getList(1)
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actPayhistoryStateful
        } else null
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

    private fun getList(page: Int) {
        showLoading()
        HttpNetWork.getInstance().haliaeetusApi.getTransactionRecords(page, 10).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
                resetRefresh()
                closeLoading()
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    mHistory = JsonUtils.fromJsonDate(mResponse.data, MPayHistoryAll::class.java)
                    initData()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
                resetRefresh()
            }
        })
    }

    private fun initData() {
        if (mHistory == null) {
            return
        }
        if (mPayHistory == null) {
            mPayHistory = ArrayList()
        }
        if (mHistory!!.current_page == 1) {
            mPayHistory.clear()
        }
        mPayHistory.addAll(mHistory!!.transaction_records)

        if (mPayHistory.size == 0) {
            showEmpty("", "暂无数据")
            return
        } else {
            showContent()
        }
        if (mAdapter == null) {
            val llm = LinearLayoutManager(this@PayHistoryActivity)
            binding.actPayhistoryRecycler.layoutManager = llm
            binding.actPayhistoryRecycler.setHasFixedSize(true)
            val variableId = BR.payHistoryBean
            mAdapter = object : CommonRecyclerViewAdapter<MPayHistory>(R.layout.item_pay_history, variableId,
                    mPayHistory
            ) {
                override fun convert(holder: CommonItemViewHolder, position: Int, payHistory: MPayHistory) {
                    holder.setBinding(variableId,payHistory)
                }
            }

            mHeader = HeaderAndFooterAdapter(mAdapter)
            binding.actPayhistoryRecycler.adapter = mHeader
        } else {
            mAdapter!!.setDatas(mPayHistory)
            mHeader.notifyDataSetChanged()
        }

        if (mHistory!!.current_page * mHistory!!.per_page < mHistory!!.total) {
            binding.actPayhistoryRefresh.setOnPushLoadMoreListener(this)
            removeFootViewForRecycler(mHeader)
        } else {
            addFootEmptyView(mHeader)
            binding.actPayhistoryRefresh.setOnPushLoadMoreListener(null)
        }
    }

    /**
     * 刷新或者加载更多回来时候需要关闭
     *
     * @author dim.
     * @time 2018/6/15 14:54.
     */
    private fun resetRefresh() {
        closeLoading()
        if (binding.actPayhistoryRefresh.isRefreshing) {
            binding.actPayhistoryRefresh.isRefreshing = false
        }
        if (binding.actPayhistoryRefresh.isLoadMore) {
            binding.actPayhistoryRefresh.isLoadMore = false
        }
    }

    override fun onLoadMore() {
        getList(mHistory!!.current_page + 1)
    }

    override fun onLoadMorePushDistance(distance: Int) {

    }

    override fun onLoadMorePushEnable(enable: Boolean) {

    }

    override fun onRefresh() {
        getList(1)
    }

    override fun onRefreshPullDistance(distance: Int) {

    }

    override fun onRefreshPullEnable(enable: Boolean) {

    }
}