package ishangchao.cn.haliaeetus.selfcenter.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/15 14:48
 * 邮箱：271756926@qq.com
 */
class MPayHistoryAll : Serializable {

    var transaction_records = ArrayList<MPayHistory>()
    var current_page = 1
    var pages = 1
    var total = 0
    var per_page = 0

}