package ishangchao.cn.haliaeetus.selfcenter.model

import java.io.Serializable

/**
 * @author by dim
 * @data 2018/6/14 11:46
 * 邮箱：271756926@qq.com
 */
class MSelfCenterProfile : Serializable {
    var id: Int = 0
    var nickname: String = ""
    var avatar_url: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var email: String = ""
    var us_phone: String = ""
    var balance: String = ""

}