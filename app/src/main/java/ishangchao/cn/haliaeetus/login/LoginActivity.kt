package ishangchao.cn.haliaeetus.login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import com.kalemao.library.http.JsonUtils
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActLoginBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.login.event.MessageLoginEvent
import ishangchao.cn.haliaeetus.login.event.MessageRegiestEvent
import ishangchao.cn.haliaeetus.login.event.MessageResetPwd
import ishangchao.cn.haliaeetus.login.model.MRegiest
import ishangchao.cn.haliaeetus.utils.User
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.HashMap

/**
 * @author by dim
 * @data 2018/6/14 15:57
 * 邮箱：271756926@qq.com
 */
class LoginActivity : HaliaeetusActivity() {
    lateinit var binding: ActLoginBinding

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_login
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActLoginBinding
        mStatefullLayout = binding.actLoginStateful

        binding.actLoginTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.setMyClick {
            when (it.id) {
                R.id.act_login_login -> {
                    doLogin()
                }
                R.id.act_login_forget_pwd -> {
                    doForgetPwd()
                }
                R.id.act_login_regiest -> {
                    doRegiest()
                }
            }

        }
    }

    override fun initViewEnd() {
        super.initViewEnd()
        EventBus.getDefault().register(this)
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true

    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actLoginStateful
        } else null
    }

    /**
     * 登录操作
     *
     * @author dim.
     * @time 2018/6/15 13:09.
     */
    private fun doLogin() {
        var phone = binding.actLoginPhone.text.toString()
        var pwd = binding.actLoginPwd.text.toString()
        if (TextUtils.isEmpty(phone)) {
            toast("请输入手机号")
            return
        }
        if (TextUtils.isEmpty(pwd)) {
            toast("请输入密码")
            return
        }
        showLoading()
        var hashMap: HashMap<String, String> = HashMap()
        hashMap["account"] = phone
        hashMap["password"] = pwd
        HttpNetWork.getInstance().haliaeetusApi.sessionsLogin(hashMap).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    var regiest = JsonUtils.fromJsonDate(mResponse.data, MRegiest::class.java)
                    User.instance.userLogin(regiest.private_token, this@LoginActivity)
                    EventBus.getDefault().post(MessageRegiestEvent(true))
                    this@LoginActivity.finish()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    /**
     * 注册回调
     *
     * @author dim.
     * @time 2018/6/15 13:09.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRegiested(event: MessageRegiestEvent) {
        if (event != null && event.isRegiest) {
            EventBus.getDefault().post(MessageLoginEvent(true))
            this@LoginActivity.finish()
        }
    }

    /**
     * 修改密码返回的
     *
     * @author dim.
     * @time 2018/6/15 13:19.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onResetPwd(event: MessageResetPwd) {
        if (event != null) {
            if (!TextUtils.isEmpty(event.mobile)) {
                binding.actLoginPhone.setText(event.mobile)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    /**
     * 去忘记密码界面
     *
     * @author dim.
     * @time 2018/6/15 13:09.
     */
    private fun doForgetPwd() {
        var intent = Intent()
        intent.setClass(this@LoginActivity, ForgetPwdActivity::class.java)
        this@LoginActivity.startActivity(intent)
    }

    /**
     * 去注册界面
     *
     * @author dim.
     * @time 2018/6/15 13:09.
     */
    private fun doRegiest() {
        var intent = Intent()
        intent.setClass(this@LoginActivity, RegiestActivity::class.java)
        this@LoginActivity.startActivity(intent)
    }
}