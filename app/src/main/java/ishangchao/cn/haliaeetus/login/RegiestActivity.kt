package ishangchao.cn.haliaeetus.login

import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import com.kalemao.library.base.MResponse
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.custom.topbar.KLMTopBarContract.IKLMTopBarView
import com.kalemao.library.http.BaseObserver
import com.kalemao.library.http.JsonUtils
import ishangchao.cn.haliaeetus.R
import ishangchao.cn.haliaeetus.base.HaliaeetusActivity
import ishangchao.cn.haliaeetus.databinding.ActRegiestBinding
import ishangchao.cn.haliaeetus.http.HttpNetWork
import ishangchao.cn.haliaeetus.login.event.MessageRegiestEvent
import ishangchao.cn.haliaeetus.login.model.MRegiest
import ishangchao.cn.haliaeetus.utils.User
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.HashMap


/**
 * @author by dim
 * @data 2018/6/15 09:58
 * 邮箱：271756926@qq.com
 */
class RegiestActivity : HaliaeetusActivity() {
    lateinit var binding: ActRegiestBinding

    override fun getContentViewLayoutID(): Int {
        return R.layout.act_regiest
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        binding = getDataBinding() as ActRegiestBinding
        mStatefullLayout = binding.actRegiestStateful

        binding.actRegiestTopbar.setKLMTopBarPresent(object : IKLMTopBarView {
            override fun onLeftClick() {
                onBackPressed()
            }

            override fun onRightClick() {

            }

            override fun onRightLeftClick() {

            }
        })

        binding.setMyClick {
            when (it.id) {
                R.id.act_regiest_code_get -> {
                    doGetCode()
                }
                R.id.act_regiest_regiest -> {
                    doRegiest()
                }
            }

        }
    }

    override fun setStatefulLayout(): StatefulLayout? {
        return if (binding != null) {
            binding.actRegiestStateful
        } else null
    }

    override fun beforeInit() {
        super.beforeInit()
        doesUseDataBanding = true
    }

    private fun doGetCode() {
        var phone = binding.actRegiestPhone.text.toString()
        if (TextUtils.isEmpty(phone)) {
            toast("请先输入手机号")
            return
        }
        var hashMap: HashMap<String, String> = HashMap()
        hashMap["mobile"] = phone
        showLoading()
        HttpNetWork.getInstance().haliaeetusApi.smsSignUp(hashMap).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    toast("验证码已发送")
                    doCount()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    private fun doRegiest() {
        var phone = binding.actRegiestPhone.text.toString()
        if (TextUtils.isEmpty(phone)) {
            toast("请先输入手机号")
            return
        }
        var code = binding.actRegiestCode.text.toString()
        if (TextUtils.isEmpty(code)) {
            toast("请先输入验证码")
            return
        }
        var name = binding.actRegiestName.text.toString()
        if (TextUtils.isEmpty(name)) {
            toast("请先输入昵称")
            return
        }
        var pwd = binding.actRegiestPwd.text.toString()
        if (TextUtils.isEmpty(pwd)) {
            toast("请先输入密码")
            return
        }
        showLoading()
        var hashMap: HashMap<String, String> = HashMap()
        hashMap["mobile"] = phone
        hashMap["auth_code"] = code
        hashMap["nickname"] = name
        hashMap["password"] = pwd
        HttpNetWork.getInstance().haliaeetusApi.regiestUser(hashMap).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(object : BaseObserver<MResponse>() {
            override fun onError(e: Throwable) {
                super.onError(e)
            }

            override fun onNext(mResponse: MResponse) {
                if (mResponse.doesSuccess()) {
                    var regiest = JsonUtils.fromJsonDate(mResponse.data, MRegiest::class.java)
                    User.instance.userLogin(regiest.private_token, this@RegiestActivity)
                    EventBus.getDefault().post(MessageRegiestEvent(true))
                    this@RegiestActivity.finish()
                } else {
                    toast(mResponse.biz_msg)
                }
                closeLoading()
            }
        })
    }

    private var timer: CountDownTimer? = null

    /**
     * 开始倒计时
     *
     * @author dim.
     * @time 2018/6/15 10:47.
     */
    private fun doCount() {
        /** 倒计时60秒，一次1秒 */
        timer = object : CountDownTimer((60 * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.actRegiestCodeGet.text = (String.format("%s秒后重新获取", (millisUntilFinished / 1000)))
                binding.actRegiestCodeGet.isClickable = false
                binding.actRegiestCodeGet.textColor = resources.getColor(R.color.klm_ccc)
            }

            override fun onFinish() {
                binding.actRegiestCodeGet.text = ("获取验证码")
                binding.actRegiestCodeGet.isClickable = true
                binding.actRegiestCodeGet.textColor = resources.getColor(R.color.blue)
                timer = null
            }
        }.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }
}