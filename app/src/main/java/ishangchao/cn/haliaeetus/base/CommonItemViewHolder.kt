package ishangchao.cn.haliaeetus.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import ishangchao.cn.haliaeetus.databinding.ItemSchedulesBinding


/**
 * @author by dim
 * @data 2018/3/23 15:46
 * 邮箱：271756926@qq.com
 */
class CommonItemViewHolder : RecyclerView.ViewHolder {

    private lateinit var mBinding: ViewDataBinding
    private val viewType: Int
    private lateinit var mContext: Context

    constructor(v: View, viewType: Int) : super(v) {
        this.viewType = viewType
        this.mContext = v.context
        mBinding = DataBindingUtil.bind(v)
    }

    fun setBinding(variableId: Int, `object`: Any): CommonItemViewHolder {
        mBinding.setVariable(variableId, `object`)
        mBinding.executePendingBindings()
        return this
    }

    fun setBindingWithClick(variableId: Int, `object`: Any,
            listener: OnViewClickListener): CommonItemViewHolder {
        val binding = mBinding as ItemSchedulesBinding
        binding.listener = listener
        binding.setVariable(variableId, `object`)
        binding.executePendingBindings()
        return this
    }

    interface OnViewClickListener {
        fun onViewClick(detailID: String)
    }
}