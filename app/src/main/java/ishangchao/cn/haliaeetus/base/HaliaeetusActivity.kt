package ishangchao.cn.haliaeetus.base
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.view.View
import com.kalemao.library.base.BaseActivity
import com.kalemao.library.base.BaseViewNoMore
import com.kalemao.library.custom.recycler_headandfoot.HeaderAndFooterAdapter
import com.kalemao.library.custom.stateful.StatefulLayout
import com.kalemao.library.utils.ActivityManager

/**
 * Created by junlihou on 2018/6/7.
 */
abstract class HaliaeetusActivity : BaseActivity(){
    /**
     * 通用的加载的布局
     */
    protected var mStatefullLayout: StatefulLayout? = null
    /**
     * databanding的通用对象，UI中用到的时候需要强转为当前的
     */
    // protected ViewDataBinding mDataBanding;

    /**
     * 应用退出，结束所有的activity(除了当前不关闭)
     */
    fun exit() {
        ActivityManager.getInstance().finishAllActivity()
    }

    protected override fun initViewEnd() {
        super.initViewEnd()
    }

    protected override fun onResume() {
        super.onResume()
        initStatefullLayout()
    }

    protected override fun beforeInit() {
        super.beforeInit()
    }

    /**
     * 设置StatefullLayout的资源初始化,在加载完bingding资源之后，调用下
     *
     * @author dim.
     * @time 2018/1/15 10:36.
     */
    fun initStatefullLayout() {
        if (setStatefulLayout() != null) {
            mStatefullLayout = setStatefulLayout()
        }
    }

    /**
     * 设置StatefullLayout的资源
     *
     * @return * StatefulLayout
     * @author dim.
     * @time 2018/1/15 10:36.
     */
    abstract fun setStatefulLayout(): StatefulLayout?

    /**
     * 如果使用了databanding，那么就不使用ButterKnife
     *
     * @author dim.
     * @time 2017/11/15 11:16.
     */
    protected override fun initView(savedInstanceState: Bundle?) {

    }

    protected fun getDataBinding(): ViewDataBinding {
        return DataBindingUtil.setContentView(this, getContentViewLayoutID())
    }

    /**
     * 返回逻辑，优先关闭菊花，其次其他的一些UI的操作导致不需要关闭，执行doesNeedDoForFinish，最后finish
     *
     * @author dim.
     * @time 2017/11/15 11:16.
     */
    override fun onBackPressed() {
        // 关闭菊花
        if (closeLoading()) {

        } else if (doesNeedDoForFinish()) {

        } else {
            this.finish()
        }// 其他的一些UI的操作导致不需要关闭
    }

    /**
     * 关闭页面之前是否需要处理的一些逻辑
     */
    open protected fun doesNeedDoForFinish(): Boolean {
        return false
    }

    /**
     * 关闭加载的菊花，返回true说明关闭了。返回false说明当前菊花没有显示
     */
    protected fun closeLoading(): Boolean {
        if (mStatefullLayout != null &&
                mStatefullLayout!!.options != null &&
                mStatefullLayout!!.options.isLoading) {
            mStatefullLayout!!.showContentNoAnimal()
            return true
        }
        return false
    }

    /**
     * 显示加载的菊花
     */
    protected fun showLoading(showMsg: String) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showLoading(showMsg)
        }
    }

    protected fun showLoading() {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showLoading("")
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty("", showMsg)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, showMsgEx: String) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty(showMsg, showMsgEx)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, showMsgEx: String, imageResource: Int) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty(showMsg, showMsgEx, imageResource)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, buttonMsg: String, listener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty("", showMsg, buttonMsg, listener)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, showMsgEx: String, buttonMsg: String, listener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty(showMsg, showMsgEx, buttonMsg, listener)
        }
    }

    protected fun showEmpty(showMsg: String, showMsgEx: String, imageResource: Int, buttonMsg: String,
            listener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty(showMsg, showMsgEx, imageResource, buttonMsg, listener)
        }
    }

    /**
     * 显示错误信息
     */
    protected fun showError(showMsg: String, clickListener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showError(showMsg, clickListener)
        }
    }

    /**
     * 显示网络问题
     */
    protected fun showOffline(showMsg: String, clickListener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showOffline(showMsg, clickListener)
        }
    }

    /**
     * 显示
     */
    protected fun showLocationOff(showMsg: String, clickListener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showLocationOff(showMsg, clickListener)
        }
    }

    /**
     * 显示正文内容
     */
    protected fun showContent() {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showContentNoAnimal()
        }
    }

    /**
     * 是否正在显示正文内容
     *
     * @author dim.
     * @time 2017/12/14 15:14.
     */
    protected fun doesShowError(): Boolean {
        return if (mStatefullLayout != null) {
            mStatefullLayout!!.doesShowError()
        } else true
    }

    /**
     * 为recycler增加一个footview，显示没有数据
     */
    fun addFootEmptyView(header: HeaderAndFooterAdapter<*>) {
        if (header.footersCount == 0) {
            val nomore = BaseViewNoMore(this)
            header.addFootView(nomore)
            header.notifyDataSetChanged()
        }
    }

    /**
     * 删除recycler底部的没有更多
     */
    fun removeFootViewForRecycler(header: HeaderAndFooterAdapter<*>?) {
        if (header != null && header.footersCount > 0) {
            header.removeFootView(0)
            header.notifyDataSetChanged()
        }
    }
}