package ishangchao.cn.haliaeetus.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * @author by dim
 * @data 2018/3/23 15:51
 * 邮箱：271756926@qq.com
 */
public abstract class CommonRecyclerViewAdapter<T>(var mLayoutId: Int, var mVariableId: Int,
        var mDatas: ArrayList<T>) : RecyclerView.Adapter<CommonItemViewHolder>() {

    fun setDatas(datas: ArrayList<T>) {
        mDatas = datas
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommonItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(mLayoutId, parent, false)
        return CommonItemViewHolder(v, mLayoutId)
    }

    override fun onBindViewHolder(holder: CommonItemViewHolder, position: Int) {
        convert(holder, position, mDatas[position])
    }

    abstract fun convert(holder: CommonItemViewHolder, position: Int, t: T)

    override fun getItemCount(): Int {
        return mDatas.size
    }
}