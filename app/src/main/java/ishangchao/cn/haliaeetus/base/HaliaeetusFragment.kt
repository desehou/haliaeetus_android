package ishangchao.cn.haliaeetus.base

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kalemao.library.base.BaseFragment
import com.kalemao.library.base.BaseViewNoMore
import com.kalemao.library.custom.recycler_headandfoot.HeaderAndFooterAdapter
import com.kalemao.library.custom.stateful.StatefulLayout

/**
 * Created by junlihou on 2018/6/7.
 */
abstract class HaliaeetusFragment : BaseFragment() {

    /**
     * 通用的加载的布局
     */
    protected var mStatefullLayout: StatefulLayout? = null
    /**
     * databanding的通用对象，UI中用到的时候需要强转为当前的
     */
    protected lateinit var mDataBanding: ViewDataBinding
    /**
     * 是否使用databanding。默认不使用，如果使用的话，需要在beforeInit（）中设置：doesUseDataBanding = true;
     */
    protected var doesUseDataBanding = false
    /**
     * 对应的Activity
     */
    protected lateinit var mActivity: Activity

    /**
     * 获得全局的，防止使用getActivity()为空
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mActivity = context as Activity
    }

    /**
     * 设置StatefullLayout的资源
     *
     * @return * StatefulLayout
     * @author dim.
     * @time 2018/1/15 10:36.
     */
    abstract fun setStatefulLayout(): StatefulLayout?

    override fun onResume() {
        super.onResume()
        initStatefullLayout()
    }

    /**
     * 设置StatefullLayout的资源初始化,在加载完bingding资源之后，调用下
     *
     * @author dim.
     * @time 2018/1/15 10:36.
     */
    fun initStatefullLayout() {
        if (setStatefulLayout() != null) {
            mStatefullLayout = setStatefulLayout()
        }
    }

    /**
     * 如果使用了databanding，那么就不使用ButterKnife
     *
     * @author dim.
     * @time 2017/11/15 11:16.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        beforeInit()
        if (doesUseDataBanding) {
            mDataBanding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
            val view = mDataBanding.root
            initView(view, savedInstanceState)
            initStatefullLayout()
            return view
        } else {
            val view = LayoutInflater.from(mActivity).inflate(getLayoutId(), container, false)
            initView(view, savedInstanceState)
            return view
        }
    }

    /**
     * UI渲染前需要设置的一些，如果使用databanding的话，需要在beforeInit（）中将这个值置为true
     *
     * @author dim.
     * @time 2017/11/15 11:20.
     */
    open protected fun beforeInit() {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    /**
     * 该抽象方法就是 onCreateView中需要的layoutID
     */
    protected abstract fun getLayoutId(): Int

    /**
     * 该抽象方法就是 初始化view
     */
    open protected fun initView(view: View, savedInstanceState: Bundle?) {

    }

    /**
     * 执行数据的加载
     */
    protected abstract fun initData()

    /**
     * 关闭加载的菊花，返回true说明关闭了。返回false说明当前菊花没有显示
     */
    protected fun closeLoading(): Boolean {
        if (mStatefullLayout != null &&
                mStatefullLayout!!.options != null &&
                mStatefullLayout!!.options.isLoading) {
            mStatefullLayout!!.showContentNoAnimal()
            return true
        }
        return false
    }

    /**
     * 显示加载的菊花
     */
    protected fun showLoading(showMsg: String) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showLoading(showMsg)
        }
    }

    protected fun showLoading() {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showLoading("")
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty("", showMsg)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, imageResource: Int) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty("", showMsg, imageResource)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, showMsgEx: String) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty(showMsg, showMsgEx)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, buttonMsg: String, listener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty("", showMsg, buttonMsg, listener)
        }
    }

    /**
     * 显示空页面
     */
    protected fun showEmpty(showMsg: String, showMsgEx: String, buttonMsg: String, listener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showEmpty(showMsg, showMsgEx, buttonMsg, listener)
        }
    }

    /**
     * 显示错误信息
     */
    protected fun showError(showMsg: String, clickListener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showError(showMsg, clickListener)
        }
    }

    /**
     * 显示网络问题
     */
    protected fun showOffline(showMsg: String, clickListener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showOffline(showMsg, clickListener)
        }
    }

    /**
     * 显示
     */
    protected fun showLocationOff(showMsg: String, clickListener: View.OnClickListener) {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showLocationOff(showMsg, clickListener)
        }
    }

    /**
     * 显示正文内容
     */
    protected fun showContent() {
        if (mStatefullLayout != null) {
            mStatefullLayout!!.showContentNoAnimal()
        }
    }

    /**
     * 为recycler增加一个footview，显示没有数据
     */
    fun addFootEmptyView(header: HeaderAndFooterAdapter<*>) {
        if (header.footersCount == 0) {
            val nomore = BaseViewNoMore(activity)
            header.addFootView(nomore)
            header.notifyDataSetChanged()
        }
    }

    /**
     * 删除recycler底部的没有更多
     */
    fun removeFootViewForRecycler(header: HeaderAndFooterAdapter<*>?) {
        if (header != null && header.footersCount > 0) {
            header.removeFootView(0)
            header.notifyDataSetChanged()
        }
    }
}